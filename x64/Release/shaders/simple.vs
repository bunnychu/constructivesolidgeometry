cbuffer CBPerObjectBuffer
{
	float4x4 WVP;
	float4x4 World;
	float4 color;
}; 

struct VSInput
{
	float4 inPos : POSITION;
	float4 inNormal : NORMAL;
	float4 inColor : COLOR;
};

struct PSInput
{
	float4 inPos : SV_POSITION;
	float3 inNormal : NORMAL;
	float4 inColor : COLOR;
};

PSInput VS(VSInput input) 
{
	PSInput output;

	output.inPos = mul(input.inPos, WVP);
	output.inNormal = float4(normalize(mul(input.inNormal, World).xyz), 1.0f);

	output.inColor = color;

	return output;
}

