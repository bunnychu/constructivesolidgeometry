#ifndef CSG_UI_UIMANAGER_H
#define CSG_UI_UIMANAGER_H

#include "gfx/common.h"

CSG_BEGIN_NAMESPACE

struct BSPNode;
class Drawable;

class UIManager
{
private:
	UIManager();
	static UIManager instance;

	int buttonWidth;
	int buttonHeight;

	int buttonSpacingX;
	int buttonSpacingY;

	int offsetX;
	int offsetY;

	XMFLOAT4 colorsArray[15];
	int colorsArraySize;
	int colorsCount;

	vector<Drawable*> lastSelectedObjects;
	Drawable* lastDrawable;

public:
	inline static UIManager& GetInstance() { return instance; }

	void CreateUIElements();
	void HandleUIElementsInput(ButtonID buttonID);

	XMFLOAT4 GetRandomColor();

	void ExecuteCSGOperation(BSPNode* (*operation)(const BSPNode&, const BSPNode&));
	void UndoCSGOperation();
};

CSG_END_NAMESPACE

#endif //CSG_UI_UIMANAGER_H