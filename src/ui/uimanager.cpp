#include "uimanager.h"
#include "primitives/tetrahedron.h"
#include "primitives/cube.h"
#include "primitives/cylinder.h"
#include "primitives/sphere.h"
#include "csg/bsp.h"
#include "gfx/scene.h"
#include "input/objectselector.h"

CSG_BEGIN_NAMESPACE

UIManager UIManager::instance;

UIManager::UIManager()
	: buttonWidth(0)
	, buttonHeight(0)
	, buttonSpacingX(5)
	, buttonSpacingY(5)
	, offsetX(15)
	, offsetY(35)
	, colorsArraySize(15)
	, colorsCount(15)
{
	int sidePanelWidth = g_WindowWidth - g_RendererWidth;
	buttonWidth = (sidePanelWidth - buttonSpacingX * 3 - offsetX) / 2;
	buttonHeight = (g_WindowHeight - buttonSpacingY * 10 - offsetY) / 10;

	colorsArray[0] = { 250.f / 255.f, 69.f / 255.f, 0.f, 1.0f }; // orange red
	colorsArray[1] = { 220.f/255.f, 20.f / 255.f, 60.f / 255.f, 1.0f }; // crimson
	colorsArray[2] = { 186.f / 255.f, 85.f / 255.f, 211.f / 255.f, 1.0f }; // medium orchid
	colorsArray[3] = { 139.f / 255.f, 0.f, 139.f / 255.f, 1.0f }; // dark magenta
	colorsArray[4] = { 30.f / 255.f, 144.f / 255.f, 255.f / 255.f, 1.0f }; // dodger blue
	colorsArray[5] = { 127.f / 255.f, 255.f / 255.f, 255.f / 212.f, 1.0f }; // aqua marine 
	colorsArray[6] = { 124.f / 255.f, 252.f / 255.f, 0.f, 1.0f }; // lawn green
	colorsArray[7] = { 0.f, 206.f / 255.f, 208.f / 255.f, 1.0f }; // dark turquoise
	colorsArray[8] = { 64.f / 255.f, 224.f / 255.f, 208.f / 255.f, 1.0f }; // turquoise
	colorsArray[9] = { 130.f / 255.f, 0.f, 130.f / 255.f, 1.0f }; // indigo
	colorsArray[10] = { 255.f / 255.f, 20.f / 255.f, 147.f / 255.f, 1.0f }; // pink
	colorsArray[11] = { 255.f / 255.f, 215.f / 255.f, 0.f, 1.0f }; // gold
	colorsArray[12] = { 255.f / 255.f, 99.f / 255.f, 71.f / 255.f, 1.0f }; // tomato
	colorsArray[13] = { 139.f / 255.f, 0.f, 0.f, 1.0f }; // dark red
	colorsArray[14] = { 0.f, 8.f / 255.f, 128.f / 255.f, 1.0f }; // navy blue
}

void UIManager::CreateUIElements()
{
	int row = 0;
	CreateWindowEx(NULL, L"button", L"Tetrahedron",
					WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON,
					g_RendererWidth + buttonSpacingX, buttonSpacingY + row * buttonHeight,
					buttonWidth, buttonHeight,
					g_Hwnd, (HMENU)ButtonID::BTN_ID_TETRAHEDRON,
					NULL, NULL);

	CreateWindowEx(NULL, L"button", L"Cube",
					WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON,
					g_RendererWidth + buttonWidth + 2 * buttonSpacingX, buttonSpacingY + row * buttonHeight,
					buttonWidth, buttonHeight,
					g_Hwnd, (HMENU)ButtonID::BTN_ID_CUBE,
					NULL, NULL);
	row = 1;

	CreateWindowEx(NULL, L"button", L"Cylinder",
					WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON,
					g_RendererWidth + buttonSpacingX, (row+1) * buttonSpacingY + row * buttonHeight,
					buttonWidth, buttonHeight,
					g_Hwnd, (HMENU)ButtonID::BTN_ID_CYLINDER,
					NULL, NULL);

	CreateWindowEx(NULL, L"button", L"Sphere",
					WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON,
					g_RendererWidth + buttonWidth + 2 * buttonSpacingX, (row + 1) * buttonSpacingY + row * buttonHeight,
					buttonWidth, buttonHeight,
					g_Hwnd, (HMENU)ButtonID::BTN_ID_SPHERE,
					NULL, NULL);
	row = 7;

	CreateWindowEx(NULL, L"button", L"Union",
					WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON,
					g_RendererWidth + buttonSpacingX, g_WindowHeight - offsetY - (row + 1) * buttonSpacingY - row * buttonHeight,
					buttonWidth * 2, buttonHeight,
					g_Hwnd, (HMENU)ButtonID::BTN_ID_UNION,
					NULL, NULL);

	row = 6;

	CreateWindowEx(NULL, L"button", L"Intersection",
					WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON,
					g_RendererWidth + buttonSpacingX, g_WindowHeight - offsetY - (row + 1) * buttonSpacingY - row * buttonHeight,
					buttonWidth * 2, buttonHeight,
					g_Hwnd, (HMENU)ButtonID::BTN_ID_INTERSECTION,
					NULL, NULL);

	row = 5;

	CreateWindowEx(NULL, L"button", L"Difference AB",
					WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON,
					g_RendererWidth + buttonSpacingX, g_WindowHeight - offsetY - (row + 1) * buttonSpacingY - row * buttonHeight,
					buttonWidth * 2, buttonHeight,
					g_Hwnd, (HMENU)ButtonID::BTN_ID_DIFFERENCE_AB,
					NULL, NULL);

	row = 4;

	CreateWindowEx(NULL, L"button", L"Difference BA",
					WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON,
					g_RendererWidth + buttonSpacingX, g_WindowHeight - offsetY - (row + 1) * buttonSpacingY - row * buttonHeight,
					buttonWidth * 2, buttonHeight,
					g_Hwnd, (HMENU)ButtonID::BTN_ID_DIFFERENCE_BA,
					NULL, NULL);

	row = 2;

	CreateWindowEx(NULL, L"button", L"Copy",
					WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON,
					g_RendererWidth + 2 * buttonSpacingX, g_WindowHeight - offsetY - (row + 1) * buttonSpacingY - row * buttonHeight,
					buttonWidth, buttonHeight,
					g_Hwnd, (HMENU)ButtonID::BTN_ID_COPY,
					NULL, NULL);

	CreateWindowEx(NULL, L"button", L"Delete",
					WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON,
					g_RendererWidth + buttonWidth + 2 * buttonSpacingX, g_WindowHeight - offsetY - (row + 1) * buttonSpacingY - row * buttonHeight,
					buttonWidth, buttonHeight,
					g_Hwnd, (HMENU)ButtonID::BTN_ID_DELETE,
					NULL, NULL);

	row = 1;
	CreateWindowEx(NULL, L"button", L"Undo",
					WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON,
					g_RendererWidth + 2 * buttonSpacingX, g_WindowHeight - offsetY - (row + 1) * buttonSpacingY - row * buttonHeight,
					buttonWidth * 2, buttonHeight,
					g_Hwnd, (HMENU)ButtonID::BTN_ID_UNDO,
					NULL, NULL);
}

void UIManager::HandleUIElementsInput(ButtonID buttonID)
{
	switch (buttonID)
	{
		case ButtonID::BTN_ID_TETRAHEDRON:
		{
			Tetrahedron* tetrahedron = new Tetrahedron();
			tetrahedron->SetColor(GetRandomColor());
			break;
		}
		case ButtonID::BTN_ID_CUBE:
		{
			Cube* cube = new Cube();
			cube->SetColor(GetRandomColor());
			break;
		}
		case ButtonID::BTN_ID_CYLINDER:
		{
			Cylinder* cylinder = new Cylinder();
			cylinder->SetColor(GetRandomColor());
			break;
		}
		case ButtonID::BTN_ID_SPHERE:
		{
			Sphere* sphere = new Sphere();
			sphere->SetColor(GetRandomColor());
			break;
		}
		case ButtonID::BTN_ID_UNION:
		{
			ExecuteCSGOperation(&UnionCSG);
			break;
		}
		case ButtonID::BTN_ID_INTERSECTION:
		{
			ExecuteCSGOperation(&IntersectionCSG);
			break;
		}
		case ButtonID::BTN_ID_DIFFERENCE_AB:
		{
			ExecuteCSGOperation(&SubstractionCSG);
			break;
		}
		case ButtonID::BTN_ID_DIFFERENCE_BA:
		{
			ExecuteCSGOperation(&InvSubstractionCSG);
			break;
		}
		case ButtonID::BTN_ID_UNDO:
		{
			UndoCSGOperation();
			break;
		}
		case ButtonID::BTN_ID_COPY:
		{
			ObjectSelector::GetInstance().CloneSelectedObjects();
			break;
		}
		case ButtonID::BTN_ID_DELETE:
		{
			ObjectSelector::GetInstance().DeleteSelectedObjects();
			break;
		}
		case ButtonID::BTN_COUNT:
			break;
		default:
			break;
	}
}

XMFLOAT4 UIManager::GetRandomColor()
{
	XMFLOAT4 retValue;
	XMFLOAT4 temp;

	if (colorsCount == 0)
	{
		colorsCount = colorsArraySize;
	}
	
	int index = rand() % colorsCount;
	retValue = colorsArray[index];
	temp = colorsArray[index];
	colorsArray[index] = colorsArray[colorsCount - 1];
	colorsArray[colorsCount - 1] = temp;
	colorsCount--;
	
	return retValue;
}

void UIManager::ExecuteCSGOperation(BSPNode* (*operation)(const BSPNode&, const BSPNode&))
{
	vector<Drawable*> selectedObjects = ObjectSelector::GetInstance().GetSelectedObjects();
	lastSelectedObjects = selectedObjects;

	BSPNode* resultCSG = nullptr;

	for (int i = 1; i < selectedObjects.size(); i++)
	{
		if (i == 1)
		{
			BSPNode leftHand(selectedObjects[i - 1]->ToPolygons());
			BSPNode rightHand(selectedObjects[i]->ToPolygons());
			resultCSG = operation(leftHand, rightHand);
		}
		else
		{
			BSPNode rightHand(selectedObjects[i]->ToPolygons());
			BSPNode* tempResult = operation(*resultCSG, rightHand);
			delete resultCSG;
			resultCSG = tempResult;
		}

		if (resultCSG == nullptr)
		{
			break;
		}
	}

	if (resultCSG != nullptr)
	{
		Drawable* drawable = new Drawable(resultCSG->GetAllPolygons());
		drawable->SetColor(selectedObjects[0]->GetColor());

		delete resultCSG;

		lastDrawable = drawable;

		drawable->GetTransform().SetPosition(drawable->GetBoundingVolume().GetCenter());

		for (auto& selectedObject : selectedObjects)
		{
			selectedObject->SetIsVisible(false);
		}

		ObjectSelector::GetInstance().AddSelectedObject(drawable);
	}
}

// TODO: Delete invisible objects
void UIManager::UndoCSGOperation()
{
	if (lastDrawable == nullptr || lastSelectedObjects.size() == 0)
	{
		return;
	}

	ObjectSelector::GetInstance().AddSelectedObject(nullptr);

	Scene::GetInstance().DeleteObject(lastDrawable);
	lastDrawable = nullptr;

	for (int i = 0; i < lastSelectedObjects.size(); i++)
	{
		ObjectSelector::GetInstance().AddSelectedObject(lastSelectedObjects[i], false);
		lastSelectedObjects[i]->SetIsVisible(true);
	}

	lastSelectedObjects.clear();
}

CSG_END_NAMESPACE
