#ifndef CSG_PRIMITIVES_CYLINDER_H
#define CSG_PRIMITIVES_CYLINDER_H

#include "gfx/common.h"
#include "primitives/drawable.h"

CSG_BEGIN_NAMESPACE

class Cylinder : public Drawable
{
	using Base = Drawable;

private:
	float radius = 1.0f;
	float height = 4.0f; 
	int discRes = 10;

public:
	Cylinder();
	virtual ~Cylinder();
	virtual bool Init() override;
	virtual void Update() override;
	virtual void Draw() override;
	virtual void Release() override;
};

CSG_END_NAMESPACE

#endif //CSG_PRIMITIVES_CYLINDER_H