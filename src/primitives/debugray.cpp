#include "debugray.h"

CSG_BEGIN_NAMESPACE

DebugRay::DebugRay(XMFLOAT3 point, XMFLOAT3 direction)
{
	topology = D3D11_PRIMITIVE_TOPOLOGY_LINELIST;
	color = XMFLOAT4(0.0f, 1.0f, 0.0f, 1.0f);

	vector<Vertex> vertices;

	Vertex origin;
	origin.pos = point;

	vertices.push_back(origin);

	origin.pos = origin.pos + direction * 100.0f;

	vertices.push_back(origin);

	Base::_ctor(vertices);
}

DebugRay::~DebugRay()
{
}

bool DebugRay::Init()
{
	return Base::Init();
}

void DebugRay::Update()
{
	Base::Update();
}

void DebugRay::Draw()
{
	Base::Draw();
}

void DebugRay::Release()
{
	Base::Release();
}

CSG_END_NAMESPACE