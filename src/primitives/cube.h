#ifndef CSG_PRIMITIVES_CUBE_H
#define CSG_PRIMITIVES_CUBE_H

#include "gfx/common.h"
#include "primitives/drawable.h"

CSG_BEGIN_NAMESPACE

class Cube : public Drawable
{
	using Base = Drawable;

private:
	static int index;

public:
	Cube();
	virtual ~Cube() override;
	virtual bool Init() override;
	virtual void Update() override;
	virtual void Draw() override;
	virtual void Release() override;
};

CSG_END_NAMESPACE
#endif //CSG_PRIMITIVES_CUBE_H