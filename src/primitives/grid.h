#ifndef CSG_PRIMITIVES_GRID_H
#define CSG_PRIMITIVES_GRID_H

#include "gfx/common.h"
#include "primitives/drawable.h"
#include "csg/plane.h"

CSG_BEGIN_NAMESPACE

class Grid : public Drawable
{
	using Base = Drawable;

private:
	int lineCountHorizontal;
	int lineCountVertical;
	float lineSpacing;

	vector<Vertex> CreateGrid(int lineCountHorizontal, int lineCountVertical, float spacing);

public:
	Grid(int lineCountHorizontal, int lineCountVertical, float lineSpacing);
	Grid(const Plane& plane);

	~Grid();

	virtual bool Init() override;
	virtual void Update() override;
	virtual void Draw() override;
	virtual void Release() override;
};

CSG_END_NAMESPACE
#endif // !CSG_PRIMITIVES_GRID_H
