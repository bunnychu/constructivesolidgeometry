#include "tetrahedron.h"
#include "csg/polygon.h"

CSG_BEGIN_NAMESPACE

Tetrahedron::Tetrahedron()
{
	Vertex v[] = 
	{
		{ { 1.0f, 1.0f,  1.0f },{ 1.0f, 0.0f, 0.0f, 1.0f } },
		{ { 1.0f, -1.0f, -1.0f },{ 0.0f, 1.0f, 0.0f, 1.0f } },
		{ { -1.0f, 1.0f, -1.0f },{ 0.0f, 0.0f, 1.0f, 1.0f } },
		{ { -1.0f, -1.0f, 1.0f },{ 0.0f, 0.0f, 0.0f, 1.0f } }
	};

	DWORD indices[] = 
	{
		3, 1, 0,
 		3, 2, 1,
 		3, 0, 2,
 		2, 0, 1
	};

	vector<Polygon> polyList;

	for (int i = 0; i < ARRAYSIZE(indices) - 2; i += 3)
	{
		vector<Vertex> polyVertices;

		Vertex x(v[indices[i]]);
		Vertex y(v[indices[i + 1]]);
		Vertex z(v[indices[i + 2]]);

		polyVertices.push_back(x);
		polyVertices.push_back(y);
		polyVertices.push_back(z);

		polyList.push_back(Polygon(polyVertices));
	}

	Base::_ctor(polyList);
}

Tetrahedron::~Tetrahedron()
{
}

bool Tetrahedron::Init()
{
	return Base::Init();
}

void Tetrahedron::Update()
{
	Base::Update();
}

void Tetrahedron::Draw()
{
	Base::Draw();
}

void Tetrahedron::Release()
{
	Base::Release();
}

CSG_END_NAMESPACE