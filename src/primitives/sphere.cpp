#include "sphere.h"
#include "csg/polygon.h"

CSG_BEGIN_NAMESPACE

Sphere::Sphere()
{
	float x;
	float y;
	vector<Vertex> vertices;
	vector<DWORD> indices;
	float piStep = (float)M_PI / (discRes - 1);

	// Creating vertices list (first point, discRes - 2, last point)
	vertices.push_back({ {radius, 0.0f, 0.0f}, color });

	for (float i = piStep; i < M_PI - piStep * 0.5f; i += piStep)
	{
		for (float j = 0.0f; j < M_PI * 2; j += (float)M_PI * 2 / discRes)
		{
			x = radius * sin(i) * cos(j);
			y = radius * sin(i) * sin(j);

			vertices.push_back({ { radius * cos(i), x, y }, color });
		}
	}

	vertices.push_back({ {-radius, 0.0f, 0.0f}, color });
	
	// Creating geometry between first point and first disc
	for (int i = 1; i <= discRes; i++)
	{
		indices.push_back(0);
		indices.push_back(i);
		indices.push_back((i % discRes) + 1);
	}

	// Creating geometry between last disc and last point
	int lastRingIndex = (int)(vertices.size() - 2);
	int startingPoint = lastRingIndex - discRes + 1;
	for (int i = startingPoint; i <= lastRingIndex; i++)
	{
		indices.push_back((int)(vertices.size() - 1));
		indices.push_back(i != lastRingIndex ? i + 1 : startingPoint);
		indices.push_back(i);
	}

	int prevBaseIdx = 0;
	int curBaseIdx = 0;

	// Build geometry between the rings (discRes - 2) rings
	for (int i = 1; i < discRes - 2; i++)
	{
		curBaseIdx = i * discRes + 1;
		prevBaseIdx = (i - 1) * discRes + 1;

		for (int k = 0; k < discRes; k++)
		{
			int x = prevBaseIdx + k;
			int y = curBaseIdx + k;
			int z = prevBaseIdx + (k + 1) % discRes;

			//CONSOLEOUT("triangle: " << x << " " << y << " " << z);

			indices.push_back(prevBaseIdx + k);
			indices.push_back(curBaseIdx + k);
			indices.push_back(prevBaseIdx + (k + 1) % discRes);

			int a = curBaseIdx + k;
			int b = curBaseIdx + (k + 1) % discRes;
			int c = prevBaseIdx + (k + 1) % discRes;

			//CONSOLEOUT("triangle: " << a << " " << b << " " << c);

			indices.push_back(curBaseIdx + k);
			indices.push_back(curBaseIdx + (k + 1) % discRes);
			indices.push_back(prevBaseIdx + (k + 1) % discRes);
		}
	}
	
	vector<Polygon> polyList;

	for (int i = 0; i < indices.size() - 2; i += 3)
	{
		vector<Vertex> polyVertices;

		Vertex x(vertices[indices[i]]);
		Vertex y(vertices[indices[i + 1]]);
		Vertex z(vertices[indices[i + 2]]);

		polyVertices.push_back(x);
		polyVertices.push_back(y);
		polyVertices.push_back(z);

		polyList.push_back(Polygon(polyVertices));
	}
	Base::_ctor(polyList);
}

Sphere::~Sphere()
{

}

bool Sphere::Init()
{
	return Base::Init();
}

void Sphere::Update()
{
	Base::Update();
}

void Sphere::Draw()
{
	Base::Draw();
}

void Sphere::Release()
{
	Base::Release();
}

CSG_END_NAMESPACE