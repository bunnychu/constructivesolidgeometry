#include "cube.h"
#include "csg/polygon.h"

CSG_BEGIN_NAMESPACE

int Cube::index = 0;

Cube::Cube()
{
	XMFLOAT4 red = { 1.0f, 0.0f, 0.0f, 1.0f };
	XMFLOAT4 green = { 0.0f, 1.0f, 0.0f, 1.0f };
	XMFLOAT4 blue = { 0.0f, 0.0f, 1.0f, 1.0f };
	XMFLOAT4 pink = { 1.0f, 0.5f, 1.0f, 1.0f };

	static int colorIndex = 0;
	XMFLOAT4 colors[] = { red, blue, green };
	vector<Polygon> polyList;
	

	Vertex v[] =
	{
		{ { -1.0f, -1.0f, -1.0f }, colors[colorIndex] },
		{ { -1.0f, +1.0f, -1.0f }, colors[colorIndex] },
		{ { +1.0f, +1.0f, -1.0f }, colors[colorIndex] },
		{ { +1.0f, -1.0f, -1.0f }, colors[colorIndex] },
		{ { -1.0f, -1.0f, +1.0f }, colors[colorIndex] },
		{ { -1.0f, +1.0f, +1.0f }, colors[colorIndex] },
		{ { +1.0f, +1.0f, +1.0f }, colors[colorIndex] },
		{ { +1.0f, -1.0f, +1.0f }, colors[colorIndex] }
	};

	vector<Vertex> vertexList;

	colorIndex++;

	 	DWORD indices[] = 
		{
	 		// front face
	 		0, 1, 2,
	 		0, 2, 3,
	 
	 		// back face
	 		4, 6, 5,
	 		4, 7, 6,
	 
	 		// left face
	 		4, 5, 1,
	 		4, 1, 0,
	 
	 		// right face
	 		3, 2, 6,
	 		3, 6, 7,
	 
	 		// top face
	 		1, 5, 6,
	 		1, 6, 2,
	 
	 		// bottom face
	 		4, 0, 3,
	 		4, 3, 7
	 	};
	
		for (int i = 0; i < ARRAYSIZE(indices) - 2; i += 3)
		{
			vector<Vertex> polyVertices;

			Vertex x(v[indices[i]]);
			Vertex y(v[indices[i + 1]]);
			Vertex z(v[indices[i + 2]]);

			polyVertices.push_back(x);
			polyVertices.push_back(y);
			polyVertices.push_back(z);

			polyList.push_back(Polygon(polyVertices));
		}

	Base::_ctor(polyList);
}

Cube::~Cube()
{
}

bool Cube::Init()
{
	return Base::Init();
}

void Cube::Update()
{
	Base::Update();
// Keep the cubes rotating
// 	static float rot = 0.0f;
// 		
// 	rot += .00025f;
// 	if (rot > 6.28f)
// 		rot = 0.0f;
// 	
// 	XMMATRIX cube1Pos = XMMatrixTranslation(-3.0f, 0.0f, 0.0f);
// 	
// 	XMMATRIX cube2Pos = XMMatrixTranslation(3.0f, 0.0f, 0.0f);
// 	
// 	XMMATRIX pos[] = { cube1Pos, cube2Pos };
// 		
// 	transform.translation = pos[index];
// 	transform.rotation = XMMatrixRotationAxis(XMVectorSet(0.0f, 1.0f, 0.5f, 0.0f), rot);
// 	transform.scale = XMMatrixScaling( 1.0f + sin(rot), 1.0 + sin(rot), 1.0f + sin(rot));
// 	index = (index + 1) % 2;
}

void Cube::Draw()
{
	Base::Draw();
}

void Cube::Release()
{
	Base::Release();
}

CSG_END_NAMESPACE