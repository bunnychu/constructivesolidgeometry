#ifndef CSG_PRIMITIVES_DRAWABLE_H
#define CSG_PRIMITIVES_DRAWABLE_H

#include "gfx\common.h"

CSG_BEGIN_NAMESPACE

class InputHandler;
class Polygon;

class Drawable
{
private:
	bool useIndexedDrawing;
	bool isVisible;
	bool isIgnoringDepth;

protected:
	Transform transform;

	D3D11_PRIMITIVE_TOPOLOGY topology;

	//Vertices and indices
	vector<Vertex> vertices;
	vector<DWORD>  indices;
	XMFLOAT4 color;

	//Cached value of the bounding volume calculated in ctor_
	BoundingVolume cachedBV;

	//Buffer which hold our vertex data in
	ID3D11Buffer* vertexBuffer;
	ID3D11Buffer* indexBuffer;

	//Pointers to vertex and pixel shader
	ID3D11VertexShader* VS;
	ID3D11PixelShader* PS;

	//Buffers that store our shaders after compilation
	ID3D10Blob* VS_Buffer;
	ID3D10Blob* PS_Buffer;

	// Vertex layout
	ID3D11InputLayout* vertexLayout;

	ID3D11Buffer* cbPerObjectBuffer;

	struct CBPerObject
	{
		XMMATRIX WVP;
		XMMATRIX World;
		XMFLOAT4 color;
	};

	CBPerObject cbPerObject;

	void _ctor(const vector<Vertex>& _vertices);
	void _ctor(const vector<Vertex>& _vertices, const vector<DWORD>& _indices);
	void _ctor(const vector<Polygon>& polygons);

public:
	Drawable();
	Drawable(const vector<Vertex>& _vertices, const vector<DWORD>& _indices);
	Drawable(const vector<Polygon>& polygons);

	//TODO: if needed
	//Drawable(SolidObject object)
	
	virtual ~Drawable();

	InputHandler* inputHandler;

	vector<Polygon> ToPolygons();

	virtual bool Init();
	virtual void Update();
	virtual void Draw();
	virtual void Release();


	void OnKeyPress(WPARAM key, UINT keyState) const;
	void OnMousePress(WPARAM key, UINT keyState) const;
	void OnMouseMove(WPARAM key, UINT keyState) const;

	inline Transform& GetTransform() { return transform; }

	void SetColor(XMFLOAT4 colorToSet);
	inline XMFLOAT4 GetColor() { return color; }

	virtual inline void SetIsVisible(bool value) { isVisible = value; }
	virtual inline bool GetIsVisible() const { return isVisible; }

	virtual inline void SetIsIgnoringDepth(bool value) { isIgnoringDepth = value; }
	virtual inline bool GetIsIgnoringDepth() const { return isIgnoringDepth; }

	inline const BoundingVolume& GetBoundingVolume() const { return cachedBV; }

private:
	void UpdateColorAttribute();
	BoundingVolume CalculateBoundingVolume();
};

CSG_END_NAMESPACE
#endif // !CSG_PRIMITIVES_DRAWABLE_H
