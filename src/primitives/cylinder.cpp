#include "cylinder.h"
#include "csg/polygon.h"

CSG_BEGIN_NAMESPACE

Cylinder::Cylinder()
{
	int baseIdx = 0;
	float x;
	float y;
	Vertex topCenter = { {0.0f, height / 2.0f, 0.0f}, color };
	Vertex bottomCenter = { { 0.0f, -height / 2.0f, 0.0f }, color };
	vector<Vertex> vertices;
	vector<DWORD> indices;

	vertices.push_back(topCenter);

	for (int i = 0; i < discRes; i++)
	{
		x = radius * ((float)cos(2 * M_PI  * ((float)i / (float)discRes)));
		y = radius * ((float)sin(2 * M_PI  * ((float)i / (float)discRes)));
		
		vertices.push_back({ {x, height / 2 , y}, color });
	}

	for (int i = 1; i < vertices.size(); i++)
	{ 
		indices.push_back(0);
		indices.push_back(i);
		indices.push_back(i != 1 ? i - 1 : (int)vertices.size() - 1 );
	}

	baseIdx = static_cast<int>(vertices.size());
	vertices.push_back(bottomCenter);

	for (int i = 0; i < discRes; i++)
	{
		x = radius * ((float)cos(2 * M_PI  * ((float)i / (float)discRes)));
		y = radius * ((float)sin(2 * M_PI  * ((float)i / (float)discRes)));

		vertices.push_back({ { x, -height / 2 , y }, color });
	}

	for (int i = 1; i < vertices.size() - baseIdx; i++)
	{
		indices.push_back(baseIdx);
		indices.push_back(i != 1 ? baseIdx + i - 1 : (int)vertices.size() - 1);
		indices.push_back(baseIdx + i);
	}

	for (int i = 1; i <= discRes; i++)
	{
		indices.push_back(baseIdx + i);
		indices.push_back(i);
		indices.push_back(baseIdx + (i % discRes) + 1);

		indices.push_back(i);
		indices.push_back((i % discRes) + 1);
		indices.push_back(baseIdx + (i % discRes) + 1);
	}

	vector<Polygon> polyList;

	for (int i = 0; i < indices.size() - 2; i += 3)
	{
		vector<Vertex> polyVertices;

		Vertex x(vertices[indices[i]]);
		Vertex y(vertices[indices[i + 1]]);
		Vertex z(vertices[indices[i + 2]]);

		polyVertices.push_back(x);
		polyVertices.push_back(y);
		polyVertices.push_back(z);

		polyList.push_back(Polygon(polyVertices));
	}

	Base::_ctor(polyList);
}

Cylinder::~Cylinder()
{
}

bool Cylinder::Init()
{
	return Base::Init();
}

void Cylinder::Update()
{
	Base::Update();
}

void Cylinder::Draw()
{
	Base::Draw();
}

void Cylinder::Release()
{
	Base::Release();
}

CSG_END_NAMESPACE