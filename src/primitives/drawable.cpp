#include "drawable.h"
#include "gfx/scene.h"
#include "input/inputmanager.h"
#include "input/inputhandler.h"
#include "csg/polygon.h"

CSG_BEGIN_NAMESPACE

Drawable::Drawable()
	: inputHandler(nullptr)
	, cachedBV()
{
	useIndexedDrawing = true;
	isVisible = true;
	isIgnoringDepth = false;
	topology = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
	color = XMFLOAT4(0.5f, 0.5f, 0.5f, 1.0f);
	vertexBuffer = nullptr;
	indexBuffer = nullptr;
	VS = nullptr;
	PS = nullptr;
	VS_Buffer = nullptr;
	PS_Buffer = nullptr;
	vertexLayout = nullptr;
	cbPerObjectBuffer = nullptr;
	InputManager::GetInstance().Register(this);
}

Drawable::Drawable(const vector<Vertex>& _vertices, const vector<DWORD>& _indices) : Drawable()
{
	_ctor(_vertices, _indices);
}

Drawable::Drawable(const vector<Polygon>& polygons) : Drawable()
{
	_ctor(polygons);
}

void Drawable::_ctor(const vector<Vertex>& _vertices)
{
	useIndexedDrawing = false;

	vertices = _vertices;

	UpdateColorAttribute();
	
	//InputManager::GetInstance().Register(this);
	Scene::GetInstance().Register(this);

	Init();
}

void Drawable::_ctor(const vector<Vertex>& _vertices, const vector<DWORD>& _indices)
{
	useIndexedDrawing = true;

	if (&vertices != &_vertices)
	{
		vertices = _vertices;
	}
	
	if (&indices != &_indices)
	{
		indices = _indices;
	}

	UpdateColorAttribute();

	//InputManager::GetInstance().Register(this);
	Scene::GetInstance().Register(this);

	// Calculate bounding volume and get center
	cachedBV = CalculateBoundingVolume();
	XMFLOAT3 boundingCenter = cachedBV.GetCenter();

	// Update drawable pivot point (point to which all vertices are relative
	for (auto& v : vertices)
	{
		v.pos = v.pos - boundingCenter;
	}

	Init();
}

void Drawable::_ctor(const vector<Polygon>& polygons)
{
	int baseIdx = 0;

	for (int i = 0; i < polygons.size(); i++)
	{
		vector<Vertex> vertexPoly = polygons[i].vertices;
		for (auto& v : vertexPoly)
		{
			v.normal = polygons[i].plane.GetNormal();
		}

		vertices.insert(vertices.end(), vertexPoly.begin(), vertexPoly.end());

		for (int j = 2; j < vertexPoly.size(); j++)
		{
			indices.push_back(baseIdx);
			indices.push_back(baseIdx + j - 1);
			indices.push_back(baseIdx + j);
		}

		baseIdx += static_cast<int>(vertexPoly.size());
	}

	_ctor(vertices, indices);
}

Drawable::~Drawable()
{
	InputManager::GetInstance().Unregister(this);
	Scene::GetInstance().Unregister(this);

	if (inputHandler)
	{
		delete inputHandler;
	}

	Release();
}

void Drawable::SetColor(XMFLOAT4 colorToSet)
{
	color = colorToSet;

	UpdateColorAttribute();

	//Changing the structure of the vertex buffer requires a ReInit
	//TODO: fix this with vertex buffer USAGE_DYNAMIC or UPDATE_SUBRESOURCE
	//Release();
	//Init();
}

void Drawable::UpdateColorAttribute()
{
//  	for (int i = 0; i < vertices.size(); i++)
//  	{
//  		vertices[i].color = color;
//  	}
}

BoundingVolume Drawable::CalculateBoundingVolume()
{
	BoundingVolume boundingVolume;

	if (vertices.size() == 0)
		return boundingVolume;

	boundingVolume.minBound = transform.GetWorldMatrix() * vertices[0].pos;
	boundingVolume.maxBound = transform.GetWorldMatrix() * vertices[0].pos;

	for (int i = 1; i < vertices.size(); i++)
	{
		XMFLOAT3 worldPos = transform.GetWorldMatrix() * vertices[i].pos;

		if (boundingVolume.minBound.x > worldPos.x) boundingVolume.minBound.x = worldPos.x;
		if (boundingVolume.minBound.y > worldPos.y) boundingVolume.minBound.y = worldPos.y;
		if (boundingVolume.minBound.z > worldPos.z) boundingVolume.minBound.z = worldPos.z;

		if (boundingVolume.maxBound.x < worldPos.x) boundingVolume.maxBound.x = worldPos.x;
		if (boundingVolume.maxBound.y < worldPos.y) boundingVolume.maxBound.y = worldPos.y;
		if (boundingVolume.maxBound.z < worldPos.z) boundingVolume.maxBound.z = worldPos.z;
	}

	return boundingVolume;
}

vector<Polygon> Drawable::ToPolygons()
{
	vector<Polygon> polygons;
	
	for (int i = 0; i < indices.size(); i+=3)
	{
		vector<Vertex> polyVertices;
		
		//Clone vertices that need to be added to the poly list 
		Vertex x(vertices[indices[i]]);
		Vertex y(vertices[indices[i+1]]);
		Vertex z(vertices[indices[i+2]]);
		
		//TODO: this needs to be revized regarding the pivot problem
		// Find the vertex position in world-space, and use that (instead of local)
		x.pos = transform.GetWorldMatrix() * x.pos;
		y.pos = transform.GetWorldMatrix() * y.pos;
		z.pos = transform.GetWorldMatrix() * z.pos;
		
		// Push to the poly verticies list
		polyVertices.push_back(x);
		polyVertices.push_back(y);
		polyVertices.push_back(z);

		polygons.push_back(Polygon(polyVertices));
	}

	return polygons;
}

bool Drawable::Init()
{
	HRESULT hr;
	hr = D3DCompileFromFile(L"shaders/simple.vs", 0, 0, "VS", "vs_5_0", 0, 0, &VS_Buffer, 0);
	hr = D3DCompileFromFile(L"shaders/simple.ps", 0, 0, "PS", "ps_5_0", 0, 0, &PS_Buffer, 0);

	hr = Device::GetInstance().device->CreateVertexShader(VS_Buffer->GetBufferPointer(), VS_Buffer->GetBufferSize(), NULL, &VS);
	hr = Device::GetInstance().device->CreatePixelShader(PS_Buffer->GetBufferPointer(), PS_Buffer->GetBufferSize(), NULL, &PS);

	Device::GetInstance().deviceContext->VSSetShader(VS, 0, 0);
	Device::GetInstance().deviceContext->PSSetShader(PS, 0, 0);

	// Fill out a vertex buffer desc
	D3D11_BUFFER_DESC vertexBufferDesc;
	ZeroMemory(&vertexBufferDesc, sizeof(vertexBufferDesc));

	vertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	vertexBufferDesc.ByteWidth = static_cast<UINT>(sizeof(Vertex) * vertices.size());
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = 0;
	vertexBufferDesc.MiscFlags = 0;

	D3D11_BUFFER_DESC cbPerObjectBufferDesc;
	ZeroMemory(&cbPerObjectBufferDesc, sizeof(cbPerObjectBufferDesc));

	cbPerObjectBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	cbPerObjectBufferDesc.ByteWidth = sizeof(CBPerObject);
	cbPerObjectBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;

	// Fill out a subresource data desc
	D3D11_SUBRESOURCE_DATA vertexBufferData;
	ZeroMemory(&vertexBufferData, sizeof(vertexBufferData));
	vertexBufferData.pSysMem = vertices.data();

	// Create our vertex buffer; stored in a ID3D11Buffer*
	hr = Device::GetInstance().device->CreateBuffer(&vertexBufferDesc, &vertexBufferData, &vertexBuffer);

	// Create the Constant Buffer
	hr = Device::GetInstance().device->CreateBuffer(&cbPerObjectBufferDesc, NULL, &cbPerObjectBuffer);

	// Create the Input vertex layout (ID3D11InputLayout)
	hr = Device::GetInstance().device->CreateInputLayout(layout, ARRAYSIZE(layout), VS_Buffer->GetBufferPointer(), VS_Buffer->GetBufferSize(), &vertexLayout);

	if (useIndexedDrawing)
	{
		D3D11_BUFFER_DESC indexBufferDesc;
		ZeroMemory(&indexBufferDesc, sizeof(indexBufferDesc));
		indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
		indexBufferDesc.ByteWidth = static_cast<UINT>(sizeof(DWORD) * indices.size());
		indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
		indexBufferDesc.CPUAccessFlags = 0;
		indexBufferDesc.MiscFlags = 0;

		D3D11_SUBRESOURCE_DATA indexBufferData;
		ZeroMemory(&indexBufferData, sizeof(indexBufferData));
		indexBufferData.pSysMem = indices.data();

		hr = Device::GetInstance().device->CreateBuffer(&indexBufferDesc, &indexBufferData, &indexBuffer);
	}

	return true;
}

void Drawable::Update()
{

}

void Drawable::Draw()
{
	// Set the vertex buffer in the IA
	UINT stride = sizeof(Vertex);
	UINT offset = 0;
	Device::GetInstance().deviceContext->IASetVertexBuffers(0, 1, &vertexBuffer, &stride, &offset);

	// Set the created vertex input layout
	Device::GetInstance().deviceContext->IASetInputLayout(vertexLayout);

	// Set the primitive topology
	Device::GetInstance().deviceContext->IASetPrimitiveTopology(topology);

	//Update WVP data and set to constant buffer
	XMMATRIX world = transform.GetWorldMatrix();

	XMMATRIX view = Scene::GetInstance().GetCamera()->GetView();
	XMMATRIX projection = Scene::GetInstance().GetCamera()->GetProjection();

	cbPerObject.WVP = XMMatrixTranspose(world * view * projection);
	cbPerObject.World = world;
	cbPerObject.color = color;

	//Update constant buffer on gpu
	Device::GetInstance().deviceContext->UpdateSubresource(cbPerObjectBuffer, 0, NULL, &cbPerObject, 0, 0);
	Device::GetInstance().deviceContext->VSSetConstantBuffers(0, 1, &cbPerObjectBuffer);

	if (useIndexedDrawing)
	{
		if (isVisible)
		{
			Device::GetInstance().deviceContext->IASetIndexBuffer(indexBuffer, DXGI_FORMAT_R32_UINT, 0);
			Device::GetInstance().deviceContext->DrawIndexed(static_cast<UINT>(indices.size()), 0, 0);
		}
	}
	else
	{
		if (isVisible)
		{
			Device::GetInstance().deviceContext->Draw(static_cast<UINT>(vertices.size()), 0);
		}
	}
}

void Drawable::Release()
{
	//TODO:: camera should still initialize all these eventhough it isn't drawing anything (to remove the silly ifs)
	if (vertexBuffer)						vertexBuffer->Release();
	if (useIndexedDrawing && indexBuffer)	indexBuffer->Release();
	if (VS)									VS->Release();
	if (PS)									PS->Release();
	if (VS_Buffer)							VS_Buffer->Release();
	if (PS_Buffer)							PS_Buffer->Release();
	if (vertexLayout)						vertexLayout->Release();
	if (cbPerObjectBuffer)					cbPerObjectBuffer->Release();
}

void Drawable::OnKeyPress(WPARAM key, UINT keyState) const
{
	if (inputHandler != nullptr)
		inputHandler->OnKeyPress(key, keyState);
}

void Drawable::OnMousePress(WPARAM key, UINT keyState) const
{
	if (inputHandler != nullptr)
		inputHandler->OnMousePress(key, keyState);
}

void Drawable::OnMouseMove(WPARAM key, UINT keyState) const
{
	if (inputHandler != nullptr)
		inputHandler->OnMouseMove(key, keyState);
}


CSG_END_NAMESPACE