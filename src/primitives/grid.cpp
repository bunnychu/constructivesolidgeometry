#include "grid.h"

CSG_BEGIN_NAMESPACE

Grid::Grid(int _lineCountHorizontal, int _lineCountVertical, float _lineSpacing)
{
	topology = D3D11_PRIMITIVE_TOPOLOGY_LINELIST;

	lineCountHorizontal = _lineCountHorizontal;
	lineCountVertical = _lineCountVertical;
	lineSpacing = _lineSpacing;

	vector<Vertex> vertices = CreateGrid(lineCountHorizontal, lineCountVertical, lineSpacing);
	Base::_ctor(vertices);
}

Grid::Grid(const Plane& plane) : Grid(11, 11, 0.3f)
{
	XMVECTOR up = XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);
	XMVECTOR normal = XMLoadFloat3(&plane.GetNormal());
	XMVECTOR axis = XMVector3Cross(up, normal);

	if (XMVectorGetByIndex(XMVector3Length(axis), 0) != 0.0f)
	{
		float cosAngle = Dot(ToFloat3(up), plane.GetNormal());
		XMMATRIX mat = XMMatrixRotationAxis(axis, acosf(cosAngle));
		transform.SetRotationMat(mat);
	}
	
	XMVECTOR position = XMVectorScale(normal, plane.GetW());
	transform.SetPosition(position);
}

Grid::~Grid()
{
}

std::vector<Vertex> Grid::CreateGrid(int lineCountHorizontal, int lineCountVertical, float spacing)
{
	vector<Vertex> vertices;

	XMFLOAT3 position1 = { 0.0f, 0.0f, lineSpacing * (lineCountHorizontal / 2) };
	XMFLOAT3 position2 = { 0.0f, 0.0f, -lineSpacing * (lineCountHorizontal / 2) };

	Vertex vx = { position1, color };
	Vertex vy = { position2, color };

	vertices.push_back(vx);
	vertices.push_back(vy);

	for (int i = 1; i <= lineCountVertical; i++)
	{
		float scaler = 1.0f;

		if (i % 2 != 0)
		{
			scaler = -1.0f;
		}

		XMFLOAT3 pos1 = position1;
		pos1.x += lineSpacing * scaler * (i / 2);
		vx = { pos1, color };

		XMFLOAT3 pos2 = position2;
		pos2.x += lineSpacing * scaler * (i / 2);
		vy = { pos2, color };

		vertices.push_back(vx);
		vertices.push_back(vy);
	}


	position1 = { lineSpacing * (lineCountVertical / 2), 0.0f, 0.0f };
	position2 = { -lineSpacing * (lineCountVertical / 2), 0.0f, 0.0f };

	vx = { position1, color };
	vy = { position2, color };

	vertices.push_back(vx);
	vertices.push_back(vy);

	for (int i = 1; i <= lineCountHorizontal; i++)
	{
		float scaler = 1.0f;

		if (i % 2 != 0)
		{
			scaler = -1.0f;
		}

		XMFLOAT3 pos1 = position1;
		pos1.z += lineSpacing * scaler * (i / 2);
		vx = { pos1, color };

		XMFLOAT3 pos2 = position2;
		pos2.z += lineSpacing * scaler * (i / 2);
		vy = { pos2, color };

		vertices.push_back(vx);
		vertices.push_back(vy);
	}

	return vertices;
}

bool Grid::Init()
{
	return Base::Init();
}

void Grid::Update()
{
	Base::Update();
}

void Grid::Draw()
{
	Base::Draw();
}

void Grid::Release()
{
	Base::Release();
}

CSG_END_NAMESPACE