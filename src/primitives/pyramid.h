#ifndef CSG_PRIMITIVES_PYRAMID_H
#define CSG_PRIMITIVES_PYRAMID_H

#include "gfx/common.h"
#include "primitives/drawable.h"

CSG_BEGIN_NAMESPACE

class Pyramid : public Drawable
{
	using Base = Drawable;

public:
	Pyramid();
	virtual ~Pyramid() override;
	virtual bool Init() override;
	virtual void Update() override;
	virtual void Draw() override;
	virtual void Release() override;
};

CSG_END_NAMESPACE
#endif //CSG_PRIMITIVES_PYRAMID_H