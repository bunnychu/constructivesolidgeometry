#ifndef CSG_PRIMITIVES_DEBUGRAY_H
#define CSG_PRIMITIVES_DEBUGRAY_H

#include "gfx/common.h"
#include "primitives/drawable.h"
#include "csg/plane.h"

CSG_BEGIN_NAMESPACE

class DebugRay : public Drawable
{
	using Base = Drawable;

public:
	DebugRay(XMFLOAT3 point, XMFLOAT3 direction);
	~DebugRay();

	virtual bool Init() override;
	virtual void Update() override;
	virtual void Draw() override;
	virtual void Release() override;
};

CSG_END_NAMESPACE
#endif // !CSG_PRIMITIVES_DEBUGRAY_H
