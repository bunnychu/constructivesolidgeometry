#ifndef CSG_PRIMITIVES_SPHERE_H
#define CSG_PRIMITIVES_SPHERE_H

#include "gfx/common.h"
#include "primitives/drawable.h"

CSG_BEGIN_NAMESPACE

class Sphere : public Drawable
{
	using Base = Drawable;

private:
	float radius = 1.0f;
	int discRes = 10;
	//int discNumber = 10;

public:
	Sphere();
	virtual ~Sphere() override;
	virtual bool Init() override;
	virtual void Update() override;
	virtual void Draw() override;
	virtual void Release() override;
};

CSG_END_NAMESPACE

#endif //CSG_PRIMITIVES_SPHERE_H