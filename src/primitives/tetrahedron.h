#ifndef CSG_PRIMITIVES_TETRAHEDRON_H
#define CSG_PRIMITIVES_TETRAHEDRON_H

#include "gfx/common.h"
#include "primitives/drawable.h"

CSG_BEGIN_NAMESPACE

class Tetrahedron: public Drawable
{
	using Base = Drawable;

public:
	Tetrahedron();
	virtual ~Tetrahedron() override;
	virtual bool Init() override;
	virtual void Update() override;
	virtual void Draw() override;
	virtual void Release() override;
};

CSG_END_NAMESPACE

#endif //CSG_PRIMITIVES_TETRAHEDRON_H