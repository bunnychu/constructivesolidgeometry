#ifndef CSG_PRIMITIVES_CONTAINERS_DRAWABLECONTAINER_H
#define CSG_PRIMITIVES_CONTAINERS_DRAWABLECONTAINER_H

#include "gfx/common.h"
#include "primitives/drawable.h"

CSG_BEGIN_NAMESPACE

class DrawableContainer : public Drawable
{
	using Base = Drawable;

private:
	vector<Drawable*> drawableChildren;

public:

	// Add a drawable as a child of this container
	void AddChild(Drawable* drawable);

	virtual bool Init() override;
	virtual void Update() override;
	virtual void Draw() override;
	virtual void Release() override;

	virtual void SetIsVisible(bool value) override;
	virtual void SetIsIgnoringDepth(bool value) override;

	DrawableContainer();
	~DrawableContainer();
};

CSG_END_NAMESPACE
#endif //CSG_PRIMITIVES_CONTAINERS_DRAWABLECONTAINER_H