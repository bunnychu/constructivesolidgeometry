#include "controllerbuilder.h"
#include "primitives/cube.h"
#include "primitives/cylinder.h"
#include "primitives/pyramid.h"
#include "primitives/sphere.h"
#include "input/inputhandler.h"
#include "csg/bsp.h"

CSG_BEGIN_NAMESPACE

DrawableContainer* ControllerBuilder::BuildMoveController()
{
	DrawableContainer* moveController = new DrawableContainer();
	const float axisLength = 0.6f;
	const float axisWidth = 0.05f;
	const float markerWidth = 0.125f;

	Drawable* up = new Cylinder();
	up->GetTransform().SetPosition(0.0f, axisLength, 0.0f);
	up->GetTransform().SetScale(axisWidth, axisLength * 0.5f, axisWidth);
	up->SetColor({ 0.0f, 1.0f, 0.0f, 0.0f });

	Drawable* upMarker = new Pyramid();
	upMarker->GetTransform().SetPosition(0.0f, axisLength * 2.0f, 0.0f);
	upMarker->GetTransform().SetScale(markerWidth, markerWidth, markerWidth);
	upMarker->SetColor({ 0.0f, 1.0f, 0.0f, 0.0f });
	

	Drawable* right = new Cylinder();
	right->GetTransform().SetPosition(axisLength, 0.0f, 0.0f);
	right->GetTransform().SetRotation(0.0f, 0.0f, M_PI_2);
	right->GetTransform().SetScale(axisWidth, axisLength * 0.5f, axisWidth);
	right->SetColor({ 1.0f, 0.0f, 0.0f, 0.0f });

	Drawable* rightMarker = new Pyramid();
	rightMarker->GetTransform().SetPosition(axisLength * 2.0f, 0.0f, 0.0f);
	rightMarker->GetTransform().SetRotation(0.0f, 0.0f, -M_PI_2);
	rightMarker->GetTransform().SetScale(markerWidth, markerWidth, markerWidth);
	rightMarker->SetColor({ 1.0f, 0.0f, 0.0f, 0.0f });

	Drawable* forward = new Cylinder();
 	forward->GetTransform().SetPosition(0.0f, 0.0f, axisLength);
 	forward->GetTransform().SetRotation(M_PI_2, 0.0f, 0.0f);
 	forward->GetTransform().SetScale(axisWidth, axisLength * 0.5f, axisWidth);
 	forward->SetColor({ 0.0f, 0.0f, 1.0f, 0.0f });

	Drawable* forwardMarker = new Pyramid();
	forwardMarker->GetTransform().SetPosition(0.0f, 0.0f, axisLength * 2.0f);
	forwardMarker->GetTransform().SetRotation(M_PI_2, 0.0f, 0.0f);
	forwardMarker->GetTransform().SetScale(markerWidth, markerWidth, markerWidth);
	forwardMarker->SetColor({ 0.0f, 0.0f, 1.0f, 0.0f });

	moveController->AddChild(up);
	moveController->AddChild(upMarker);
	moveController->AddChild(right);
	moveController->AddChild(rightMarker);
	moveController->AddChild(forward);
	moveController->AddChild(forwardMarker);

	moveController->SetIsIgnoringDepth(true);
	moveController->inputHandler = new MoveControllerInputHandler(moveController);

	return moveController;
}

DrawableContainer* ControllerBuilder::BuildRotateController()
{
	DrawableContainer* rotateController = new DrawableContainer();
	const float axisLength = 0.6f;
	const float axisWidth = 0.05f;
	const float markerWidth = 0.125f;

	Drawable* up = new Cylinder();
	up->GetTransform().SetPosition(0.0f, axisLength, 0.0f);
	up->GetTransform().SetScale(axisWidth, axisLength * 0.5f, axisWidth);
	up->SetColor({ 0.0f, 1.0f, 0.0f, 0.0f });

	Drawable* upMarker = new Sphere();
	upMarker->GetTransform().SetPosition(0.0f, axisLength * 2.0f, 0.0f);
	upMarker->GetTransform().SetScale(markerWidth, markerWidth, markerWidth);
	upMarker->SetColor({ 0.0f, 1.0f, 0.0f, 0.0f });


	Drawable* right = new Cylinder();
	right->GetTransform().SetPosition(axisLength, 0.0f, 0.0f);
	right->GetTransform().SetRotation(0.0f, 0.0f, M_PI_2);
	right->GetTransform().SetScale(axisWidth, axisLength * 0.5f, axisWidth);
	right->SetColor({ 1.0f, 0.0f, 0.0f, 0.0f });

	Drawable* rightMarker = new Sphere();
	rightMarker->GetTransform().SetPosition(axisLength * 2.0f, 0.0f, 0.0f);
	rightMarker->GetTransform().SetRotation(0.0f, 0.0f, -M_PI_2);
	rightMarker->GetTransform().SetScale(markerWidth, markerWidth, markerWidth);
	rightMarker->SetColor({ 1.0f, 0.0f, 0.0f, 0.0f });

	Drawable* forward = new Cylinder();
	forward->GetTransform().SetPosition(0.0f, 0.0f, axisLength);
	forward->GetTransform().SetRotation(M_PI_2, 0.0f, 0.0f);
	forward->GetTransform().SetScale(axisWidth, axisLength * 0.5f, axisWidth);
	forward->SetColor({ 0.0f, 0.0f, 1.0f, 0.0f });

	Drawable* forwardMarker = new Sphere();
	forwardMarker->GetTransform().SetPosition(0.0f, 0.0f, axisLength * 2.0f);
	forwardMarker->GetTransform().SetRotation(M_PI_2, 0.0f, 0.0f);
	forwardMarker->GetTransform().SetScale(markerWidth, markerWidth, markerWidth);
	forwardMarker->SetColor({ 0.0f, 0.0f, 1.0f, 0.0f });

	rotateController->AddChild(up);
	rotateController->AddChild(upMarker);
	rotateController->AddChild(right);
	rotateController->AddChild(rightMarker);
	rotateController->AddChild(forward);
	rotateController->AddChild(forwardMarker);

	rotateController->SetIsIgnoringDepth(true);
	rotateController->inputHandler = new RotateControllerInputHandler(rotateController);

	return rotateController;
}

DrawableContainer* ControllerBuilder::BuildScaleController()
{
	DrawableContainer* scaleController = new DrawableContainer();
	const float axisLength = 0.6f;
	const float axisWidth = 0.05f;
	const float markerWidth = 0.1f;

	Drawable* up = new Cube();
	up->GetTransform().SetPosition(0.0f, axisLength + axisWidth, 0.0f);
	up->GetTransform().SetScale(axisWidth, axisLength, axisWidth);
	up->SetColor({ 0.0f, 1.0f, 0.0f, 0.0f });

	Drawable* upMarker = new Cube();
	upMarker->GetTransform().SetPosition(0.0f, axisLength * 2.0f, 0.0f);
	upMarker->GetTransform().SetScale(markerWidth, markerWidth, markerWidth);
	upMarker->SetColor({ 0.0f, 1.0f, 0.0f, 0.0f });

	Drawable* right = new Cube();
	right->GetTransform().SetPosition(axisLength, 0.0f, 0.0f);
	right->GetTransform().SetScale(axisLength + axisWidth, axisWidth, axisWidth);
	right->SetColor({ 1.0f, 0.0f, 0.0f, 0.0f });

	Drawable* rightMarker = new Cube();
	rightMarker->GetTransform().SetPosition(axisLength * 2.0f, 0.0f, 0.0f);
	rightMarker->GetTransform().SetRotation(0.0f, 0.0f, -M_PI_2);
	rightMarker->GetTransform().SetScale(markerWidth, markerWidth, markerWidth);
	rightMarker->SetColor({ 1.0f, 0.0f, 0.0f, 0.0f });

	Drawable* forwardMarker = new Cube();
	forwardMarker->GetTransform().SetPosition(0.0f, 0.0f, axisLength * 2.0f);
	forwardMarker->GetTransform().SetRotation(M_PI_2, 0.0f, 0.0f);
	forwardMarker->GetTransform().SetScale(markerWidth, markerWidth, markerWidth);
	forwardMarker->SetColor({ 0.0f, 0.0f, 1.0f, 0.0f });

	Drawable* forward = new Cube();
	forward->GetTransform().SetPosition(0.0f, 0.0f, axisLength + axisWidth);
	forward->GetTransform().SetScale(axisWidth, axisWidth, axisLength);
	forward->SetColor({ 0.0f, 0.0f, 1.0f, 0.0f });


	scaleController->AddChild(up);
	scaleController->AddChild(upMarker);
	scaleController->AddChild(right);
	scaleController->AddChild(rightMarker);
	scaleController->AddChild(forward);
	scaleController->AddChild(forwardMarker);

	scaleController->SetIsIgnoringDepth(true);
	scaleController->inputHandler = new ScaleControllerInputHandler(scaleController);

	return scaleController;
}

CSG_END_NAMESPACE


