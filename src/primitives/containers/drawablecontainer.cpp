#include "drawablecontainer.h"
#include "gfx/scene.h"
#include "input/inputmanager.h"

CSG_BEGIN_NAMESPACE

void CSG::DrawableContainer::AddChild(Drawable* drawable)
{
	Scene::GetInstance().Unregister(drawable);
	InputManager::GetInstance().Unregister(drawable);

	drawableChildren.push_back(drawable);
}

void DrawableContainer::SetIsVisible(bool value)
{
	Base::SetIsVisible(value);

	for (Drawable* d : drawableChildren)
	{
		d->SetIsVisible(value);
	}
}

void DrawableContainer::SetIsIgnoringDepth(bool value)
{
	Base::SetIsIgnoringDepth(value);

	for (Drawable* d : drawableChildren)
	{
		d->SetIsIgnoringDepth(value);
	}
}

CSG::DrawableContainer::DrawableContainer()
{
	Scene::GetInstance().Register(this);
}

bool DrawableContainer::Init()
{
	bool result = true;

	for (Drawable* d : drawableChildren)
		result = result & d->Init();

	return result;
}

void DrawableContainer::Update()
{
	for (Drawable* d : drawableChildren)
		d->Update();
}

void DrawableContainer::Draw()
{
	for (Drawable* d : drawableChildren)
	{
		XMMATRIX cachedTranslationMat = d->GetTransform().GetTranslationMat();

		//Convert from local to world position and draw the drawable
		d->GetTransform().SetTranslationMat(transform.GetTranslationMat() * cachedTranslationMat);

		d->Draw();

		//Set back the local position (relative to the container)
		d->GetTransform().SetTranslationMat(cachedTranslationMat);
	}
}

void DrawableContainer::Release()
{
	Base::Release();

	for (Drawable* d : drawableChildren)
		d->Release();
}

DrawableContainer::~DrawableContainer()
{
	for (Drawable* d : drawableChildren)
	{
		if(d) delete d;
	}
}

CSG_END_NAMESPACE
