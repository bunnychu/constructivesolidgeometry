#ifndef CSG_PRIMITIVES_CONTAINERS_CONTROLLERBUILDER_H
#define CSG_PRIMITIVES_CONTAINERS_CONTROLLERBUILDER_H

#include "gfx/common.h"
#include "primitives/containers/drawablecontainer.h"

CSG_BEGIN_NAMESPACE

class ControllerBuilder
{
public:
	//Cylinders instead of cubes
	static DrawableContainer* BuildMoveController();
	static DrawableContainer* BuildRotateController();
	static DrawableContainer* BuildScaleController();

	//Rotate controller 3 cylinders
	//Scale Cubes
};

CSG_END_NAMESPACE
#endif //CSG_PRIMITIVES_CONTAINERS_CONTROLLERBUILDER_H