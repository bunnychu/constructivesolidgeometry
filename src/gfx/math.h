#ifndef CSG_GFX_MATH_H
#define CSG_GFX_MATH_H
#include <DirectXMath.h>

CSG_BEGIN_NAMESPACE

using namespace DirectX;

struct BoundingVolume
{
	XMFLOAT3 minBound;
	XMFLOAT3 maxBound;

	BoundingVolume() : minBound(0.0f, 0.0f, 0.0f), maxBound(0.0f, 0.0f, 0.0f) {}

	inline XMFLOAT3 GetCenter() const
	{
		return XMFLOAT3((maxBound.x + minBound.x) * 0.5f,
						(maxBound.y + minBound.y) * 0.5f,
						(maxBound.z + minBound.z) * 0.5f);
	}
};

inline XMFLOAT3 ToFloat3(const XMVECTOR& v)
{
	XMFLOAT3 retValue;
	XMStoreFloat3(&retValue, v);
	return retValue;
}

inline XMVECTOR ToVector(const XMFLOAT3& v)
{
	return XMLoadFloat3(&v);
}

inline float Dot(XMFLOAT3 x, XMFLOAT3 y)
{
	float retValue;
	XMVECTOR vx = XMLoadFloat3(&x);
	XMVECTOR vy = XMLoadFloat3(&y);
	XMVECTOR dot = XMVector3Dot(vx, vy);

	XMStoreFloat(&retValue, dot);

	return retValue;
}

inline XMFLOAT3 Cross(XMFLOAT3 x, XMFLOAT3 y)
{
	XMFLOAT3 retValue;
	XMVECTOR vx = XMLoadFloat3(&x);
	XMVECTOR vy = XMLoadFloat3(&y);
	XMVECTOR cross = XMVector3Cross(vx, vy);

	XMStoreFloat3(&retValue, cross);

	return retValue;
}

inline XMFLOAT3 Normalize(XMFLOAT3 x)
{
	XMFLOAT3 retValue;
	XMVECTOR vx = XMLoadFloat3(&x);
	XMVECTOR normalize = XMVector3Normalize(vx);

	XMStoreFloat3(&retValue, normalize);

	return retValue;
}

inline float Length(XMFLOAT3 x)
{
	float retValue;
	XMVECTOR vx = XMLoadFloat3(&x);
	XMVECTOR length = XMVector3Length(vx);

	XMStoreFloat(&retValue, length);

	return retValue;
}

inline XMFLOAT3 operator*(const XMFLOAT3& x, float y)
{
	XMFLOAT3 retValue;
	XMVECTOR vx = XMLoadFloat3(&x);
	XMVECTOR ret = XMVectorScale(vx, y);

	XMStoreFloat3(&retValue, ret);
	return retValue;
}

inline XMFLOAT4 operator*(const XMFLOAT4& x, float y)
{
	XMFLOAT4 retValue;
	XMVECTOR vx = XMLoadFloat4(&x);
	XMVECTOR ret = XMVectorScale(vx, y);

	XMStoreFloat4(&retValue, ret);
	return retValue;
}

inline XMFLOAT3 operator*(const FXMMATRIX& m, const XMFLOAT3& x)
{
	XMFLOAT3 retValue;
	XMVECTOR vx = XMLoadFloat3(&x);
	XMVECTOR ret = XMVector3Transform(vx, m);

	XMStoreFloat3(&retValue, ret);
	return retValue;
}

inline XMFLOAT4 operator*(const FXMMATRIX& m, const XMFLOAT4& x)
{
	XMFLOAT4 retValue;
	XMVECTOR vx = XMLoadFloat4(&x);
	XMVECTOR ret = XMVector4Transform(vx, m);

	XMStoreFloat4(&retValue, ret);
	return retValue;
}

inline XMFLOAT3 operator+(const XMFLOAT3& x, const XMFLOAT3& y)
{
	XMFLOAT3 retValue;
	XMVECTOR vx = XMLoadFloat3(&x);
	XMVECTOR vy = XMLoadFloat3(&y);
	XMVECTOR sum = XMVectorAdd(vx, vy);

	XMStoreFloat3(&retValue, sum);

	return retValue;
}

inline XMFLOAT3 operator-(const XMFLOAT3& x, const XMFLOAT3& y)
{
	XMFLOAT3 retValue;
	XMVECTOR vx = XMLoadFloat3(&x);
	XMVECTOR vy = XMLoadFloat3(&y);
	XMVECTOR sub = XMVectorSubtract(vx, vy);

	XMStoreFloat3(&retValue, sub);

	return retValue;
}

inline XMFLOAT3 operator-(const XMFLOAT3& x)
{
	XMFLOAT3 retValue;
	XMVECTOR vx = XMLoadFloat3(&x);
	XMVECTOR inv = XMVectorScale(vx, -1.0f);

	XMStoreFloat3(&retValue, inv);

	return retValue;
}

inline XMFLOAT3 Lerp(const XMFLOAT3& x, const XMFLOAT3& y, float t)
 {
	return x + (y - x) * t;
}

inline Vertex Lerp(const Vertex& x, const Vertex& y, float t)
{
	Vertex ret(x);
	ret.pos = Lerp(x.pos, y.pos, t);

	return ret;
}

inline float Clamp(float x, float min, float max)
{
	return (x < min) ? min : ((x > max) ? max : x);
}

CSG_END_NAMESPACE

#endif //CSG_GFX_MATH_H