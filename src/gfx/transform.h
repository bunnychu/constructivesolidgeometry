#ifndef CSG_GFX_TRANSFORM_H
#define CSG_GFX_TRANSFORM_H

#include <DirectXMath.h>
#include <DirectXPackedVector.h>

using namespace DirectX;
using namespace DirectX::PackedVector;

struct Transform
{
private:
	XMMATRIX translation;
	XMMATRIX rotation;
	XMMATRIX scale;

	float pitch;
	float yaw;
	float roll;

public:
	Transform();

	// Setters
	void SetPosition(float x, float y, float z);
	void SetPosition(XMFLOAT3 pos);
	void SetPosition(XMVECTOR pos);

	void SetRotation(float x, float y, float z);
	void SetRotation(XMFLOAT3 rot);

	//void SetRotation(XMVECTOR axis, float rotAngle);
	void SetScale(float x, float y, float z);
	void SetScale(XMFLOAT3 sc);

	// Rotation shorthand setters
	//void SetRotationX(float rotAngle);
	//void SetRotationY(float rotAngle);
	//void SetRotationZ(float rotAngle);

	// Matrix Getters
	inline const XMMATRIX& GetTranslationMat() const { return translation; }
	inline const XMMATRIX& GetRotationMat() const { return rotation; }
	inline const XMMATRIX& GetScaleMat() const { return scale; }
	
	//Matrix Setters
	inline void SetTranslationMat(const XMMATRIX& transMat) { translation = transMat; }
	inline void SetRotationMat(const XMMATRIX& rotMat) { rotation = rotMat; }
	inline void SetScaleMat(const XMMATRIX& scaleMat) { scale = scaleMat; }

	inline const XMVECTOR GetPosition() const { return translation.r[3]; }
	
	XMFLOAT3 GetPositionFloat3() const;
	XMFLOAT3 GetRotationFloat3() const;
	XMFLOAT3 GetScaleFloat3() const;

	//Get SRT Matrix
	inline XMMATRIX GetWorldMatrix() const { return scale * rotation * translation; }
};

#endif //CSG_GFX_TRANSFORM_H