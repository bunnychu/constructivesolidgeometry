#ifndef CSG_GFX_CAMERA_H
#define CSG_GFX_CAMERA_H

#include "gfx/common.h"
#include "primitives/drawable.h"

CSG_BEGIN_NAMESPACE

class Camera : public Drawable
{
private:
	// View & proj matrices
	XMMATRIX projection;

	// Proj data
	float fovY;
	float aspectRatio;
	float nearZ;
	float farZ;

	// Viewport variable
	D3D11_VIEWPORT viewport;

	ID3D11RasterizerState* wireframe;
	ID3D11RasterizerState* msaa;

	D3D11_RASTERIZER_DESC wireframeDesc;
	D3D11_RASTERIZER_DESC msaaDesc;
	
	bool wireframeEnabled;

public:

	//Ctors
	Camera();
	~Camera();

	//Setters
	void SetProjection(float _fovY, float _aspectRatio, float _nearZ, float _farZ);

	//Getters
	XMMATRIX GetView() const;
	inline XMMATRIX GetProjection() const { return projection; }
	XMMATRIX GetInverseView() const;
	XMMATRIX GetInverseViewProjection() const;

	inline D3D11_VIEWPORT GetViewport() const { return viewport; }

	void SetWireframeEnabled(bool value);
	bool GetWireframeEnabled() const { return wireframeEnabled; }
	void Release();
};

CSG_END_NAMESPACE
#endif //CSG_GFX_CAMERA_H
