#ifndef CSG_GFX_VERTEX_H
#define CSG_GFX_VERTEX_H

#include <DirectXMath.h>
#include <DirectXPackedVector.h>

using namespace DirectX;

struct Vertex
{
	XMFLOAT3 pos;
	XMFLOAT3 normal;
	XMFLOAT4 color;


	Vertex() {}
	Vertex(float x, float y, float z, float r, float g, float b, float a) : pos(x, y, z), color(r, g, b, a) {}
	Vertex(XMFLOAT3 _pos, XMFLOAT4 _color) : pos(_pos), color(_color) {}
	//Vertex(XMFLOAT3 _pos, XMFLOAT4 _color1, XMFLOAT4 _color2) : pos(_pos), color(_color1), color1(_color2) {}

};

static D3D11_INPUT_ELEMENT_DESC layout[] =
{
	{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	{ "NORMAL", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 24, D3D11_INPUT_PER_VERTEX_DATA, 0 }
};
static UINT numElements = ARRAYSIZE(layout);

#endif //CSG_GFX_VERTEX_H