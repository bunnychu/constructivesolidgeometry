#ifndef CSG_GFX_SCENE_H
#define CSG_GFX_SCENE_H

#include "gfx/common.h"
#include "gfx/camera.h"
#include "primitives/drawable.h"

CSG_BEGIN_NAMESPACE

class Scene
{
public:

	static Scene& GetInstance() { return instance; }
	~Scene();

private:
	// Pointer to the currently used camera
	Camera* camera;

	// List of pointers to registered Drawables.
	vector<Drawable*> drawableList;

	vector<Drawable*> clientsToRemove;

	Scene();
	static Scene instance;

public:
	bool Init();
	void OnFrameStart();
	void Update();
	void Draw();
	void Release();

	void Register(Drawable* drawable);
	void Unregister(Drawable* drawable);

	void DeleteObject(Drawable* drawable);

	//Getters
	inline Camera* GetCamera() { return camera; }

	inline const vector<Drawable*> GetAllDrawables() const { return drawableList; }
};

CSG_END_NAMESPACE

#endif //!CSG_GFX_SCENE_H