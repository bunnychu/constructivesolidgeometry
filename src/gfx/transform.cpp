#include "transform.h"
#include "gfx/common.h"

Transform::Transform()
{
	translation = XMMatrixIdentity();
	rotation = XMMatrixIdentity();
	scale = XMMatrixIdentity();

	pitch = 0.0f;
	yaw = 0.0f;
	roll = 0.0f;
}

void Transform::SetPosition(float x, float y, float z)
{
	translation = XMMatrixTranslation(x, y, z);
}

void Transform::SetPosition(XMFLOAT3 pos)
{
	SetPosition(pos.x, pos.y, pos.z);
}

void Transform::SetPosition(XMVECTOR pos)
{
	XMFLOAT3 tempPos;
	XMStoreFloat3(&tempPos, pos);
	translation = XMMatrixTranslation(tempPos.x, tempPos.y, tempPos.z);
}

// void Transform::SetRotation(XMVECTOR axis, float rotAngle)
// {
// 	rotation = XMMatrixRotationAxis(axis, rotAngle);
// }

void Transform::SetRotation(float x, float y, float z)
{
	pitch = x;
	yaw = y;
	roll = z;

	//CONSOLEOUT("pitch" << pitch << "yaw" << yaw << "roll" << roll);
	rotation = XMMatrixRotationRollPitchYaw(x, y, z);
}

void Transform::SetRotation(XMFLOAT3 rot)
{
	SetRotation(rot.x, rot.y, rot.z);
}

void Transform::SetScale(float x, float y, float z)
{
	// Prevent negative scale
	float clamped_x = CSG::Clamp(x, 0.01f, SCALE_MAX_BOUND);
 	float clamped_y = CSG::Clamp(y, 0.01f, SCALE_MAX_BOUND);
 	float clamped_z = CSG::Clamp(z, 0.01f, SCALE_MAX_BOUND);

	scale = XMMatrixScaling(clamped_x, clamped_y, clamped_z);
}

void Transform::SetScale(XMFLOAT3 sc)
{
	SetScale(sc.x, sc.y, sc.z);
}

//void Transform::SetRotationX(float rotAngle)
//{
//	SetRotation(XMVectorSet(1.0f, 0.0f, 0.0f, 0.0f), rotAngle);
//}
//
//void Transform::SetRotationY(float rotAngle)
//{
//	SetRotation(XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f), rotAngle);
//}
//
//void Transform::SetRotationZ(float rotAngle)
//{
//	SetRotation(XMVectorSet(0.0f, 0.0f, 1.0f, 0.0f), rotAngle);
//}

XMFLOAT3 Transform::GetPositionFloat3() const
{
	XMFLOAT3 retValue;
	XMStoreFloat3(&retValue, translation.r[3]);

	return retValue;
}

XMFLOAT3 Transform::GetRotationFloat3() const
{
	return XMFLOAT3(pitch, yaw, roll);
}

XMFLOAT3 Transform::GetScaleFloat3() const
{
	XMFLOAT3 rows[3];
	XMStoreFloat3(&rows[0], scale.r[0]);
	XMStoreFloat3(&rows[1], scale.r[1]);
	XMStoreFloat3(&rows[2], scale.r[2]);

	return XMFLOAT3(rows[0].x, rows[1].y, rows[2].z);
}
