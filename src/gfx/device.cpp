#include "device.h"

Device* Device::instance = nullptr;

// Device::Device(HINSTANCE hInstance, HWND hwnd, int width, int height)
// {
// 	if (!Init(hInstance, hwnd, width, height))
// 	{
// 		MessageBoxA(NULL, "Failed to initialize Directx", "Failed", MB_OK);
// 	}
// 
// }

void Device::ReleaseObjects()
{
	swapChain->Release();
	device->Release();
	deviceContext->Release();
	depthStencilBuffer->Release();
	depthStencilView->Release();
}


void Device::UseDepthBuffer(bool value)
{
	if (isUsingDepthBuffer != value)
	{
		isUsingDepthBuffer = value;
		deviceContext->OMSetRenderTargets(1, &renderTargetView,  isUsingDepthBuffer ? depthStencilView : nullptr);
	}
}

bool Device::Init(HINSTANCE hInstance, HWND hwnd, int width, int height)
{
	HRESULT hr;

	// Create swap chain descriptor
	DXGI_MODE_DESC bufferDesc;

	ZeroMemory(&bufferDesc, sizeof(bufferDesc));

	bufferDesc.Width = width;
	bufferDesc.Height = height;
	bufferDesc.RefreshRate.Numerator = 60;
	bufferDesc.RefreshRate.Denominator = 1;
	bufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
	bufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	bufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;

	
	DXGI_SWAP_CHAIN_DESC swapChainDesc;
	ZeroMemory(&swapChainDesc, sizeof(swapChainDesc));

	swapChainDesc.BufferDesc = bufferDesc;
	swapChainDesc.SampleDesc.Count = 8;
	swapChainDesc.SampleDesc.Quality = 0;
	swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swapChainDesc.BufferCount = 1;
	swapChainDesc.OutputWindow = hwnd;
	swapChainDesc.Windowed = TRUE;
	swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;

	D3D11_TEXTURE2D_DESC depthStencilDesc;
	ZeroMemory(&depthStencilDesc, sizeof(depthStencilDesc));

	depthStencilDesc.Width = width;
	depthStencilDesc.Height = height;
	depthStencilDesc.MipLevels = 1;
	depthStencilDesc.ArraySize = 1;
	depthStencilDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthStencilDesc.SampleDesc.Count = 8;
	depthStencilDesc.SampleDesc.Quality = 0;
	depthStencilDesc.Usage = D3D11_USAGE_DEFAULT;
	depthStencilDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	depthStencilDesc.CPUAccessFlags = 0;
	depthStencilDesc.MiscFlags = 0;

	//hr = D3D11CreateDeviceAndSwapChain(NULL, D3D_DRIVER_TYPE_HARDWARE, NULL, NULL, NULL, NULL,
	//	D3D11_SDK_VERSION, &swapChainDesc, &swapChain, &device, NULL, &deviceContext);

	hr = D3D11CreateDeviceAndSwapChain(NULL, D3D_DRIVER_TYPE_HARDWARE, NULL, NULL, NULL, NULL,
		D3D11_SDK_VERSION, &swapChainDesc, &swapChain, &device, NULL, &deviceContext);

	hr = swapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (void**)&backBuffer);

	hr = device->CreateRenderTargetView(backBuffer, NULL, &renderTargetView);
	backBuffer->Release();

	hr = device->CreateTexture2D(&depthStencilDesc, NULL, &depthStencilBuffer);

	hr = device->CreateDepthStencilView(depthStencilBuffer, NULL, &depthStencilView);

	deviceContext->OMSetRenderTargets(1, &renderTargetView, depthStencilView);

	if (SUCCEEDED(hr))
	{
		return true;
	}

	return false;
}