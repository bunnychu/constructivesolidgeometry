#include "scene.h"
#include "gfx/common.h"
#include "csg/polygon.h"
#include "csg/bsp.h"
#include "input/objectselector.h"
#include "primitives/grid.h"

CSG_BEGIN_NAMESPACE

Scene::Scene()
	:camera(nullptr)
{
}

Scene Scene::instance;

Scene::~Scene()
{
	Release();
}

bool Scene::Init()
{
	camera = new Camera();
	ObjectSelector::GetInstance().Init();

	camera->GetTransform().SetPosition(0.0f, 3.0f, -15.0f);

	Drawable* grid = new Grid(50, 50, 1.0f);
	grid->GetTransform().SetPosition(0.0f, -1.0f, 0.0f);
	grid->SetColor(XMFLOAT4(0.2f, 0.6f, 1.0f, 1.0f));

	return true;
}

void Scene::OnFrameStart()
{
	if (clientsToRemove.size())
	{
		for (Drawable* drawable : clientsToRemove)
		{
			delete drawable;
		}
	}

	clientsToRemove.clear();
}

void Scene::Update()
{
	for (auto& drawable : drawableList)
	{
		drawable->Update();
	}
}

void Scene::Draw()
{
	vector<Drawable*> toRenderOnTop;

	for (auto& drawable : drawableList)
	{
		if (drawable->GetIsIgnoringDepth())
		{
			toRenderOnTop.push_back(drawable);
		}
		else
		{
			drawable->Draw();
		}
	}

	vector<Drawable*> selectedObjets = ObjectSelector::GetInstance().GetSelectedObjects();

 	if (selectedObjets.size() > 0)
 	{
		bool cachedWireframeState = camera->GetWireframeEnabled();
		camera->SetWireframeEnabled(true);

 		for (auto& drawable : selectedObjets)
 		{
 			XMFLOAT4 cachedObjectColor = drawable->GetColor();
 
 			drawable->SetColor(cachedObjectColor * 0.5f);
 			drawable->Draw();
 			drawable->SetColor(cachedObjectColor);
 		}

		camera->SetWireframeEnabled(cachedWireframeState);
 	}

	// Clear the depth (everything drawn after this will be on top since there is no more depth info).
	Device::GetInstance().deviceContext->ClearDepthStencilView(Device::GetInstance().depthStencilView, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);

	// If any 'on top' drawables available, draw them now
	if (toRenderOnTop.size() != 0)
	{
		for (auto& drawable : toRenderOnTop)
		{
			drawable->Draw();
		}
	}
}

void Scene::Release()
{
}

void Scene::Register(Drawable* drawable)
{
	if (find(drawableList.begin(), drawableList.end(), drawable) == drawableList.end())
	{
		drawableList.push_back(drawable);
	}
}

void Scene::Unregister(Drawable* drawable)
{
	drawableList.erase(remove(drawableList.begin(), drawableList.end(), drawable), drawableList.end());
}

void Scene::DeleteObject(Drawable* drawable)
{
	clientsToRemove.push_back(drawable);
}

CSG_END_NAMESPACE
