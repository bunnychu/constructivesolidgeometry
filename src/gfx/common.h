#ifndef CSG_DIRECTX_COMMON_H
#define CSG_DIRECTX_COMMON_H

//Namespace macro helpers
#define CSG_BEGIN_NAMESPACE namespace CSG {
#define CSG_END_NAMESPACE };

#include <iostream>
#include <sstream>
#include <vector>
#include <algorithm>

//Windows api
#include <windows.h>

//Dx11 headers
#include <d3d11.h>
#include <d3dcompiler.h>

//Xna math headers
#include <DirectXMath.h>
#include <DirectXPackedVector.h>

#include "gfx/device.h"
#include "gfx/transform.h"
#include "gfx/vertex.h"
#include "gfx/math.h"

using namespace std;
using namespace DirectX;
using namespace DirectX::PackedVector;

static LPCTSTR g_WndClassName = L"CSG";

extern HWND g_Hwnd;
extern HWND g_RendererHwnd;

static const int g_WindowWidth = 1366;
static const int g_WindowHeight = 730;

static const int g_RendererWidth = 1050;
static const int g_RendererHeight = 730;

static const float EPSILON = 0.001f;

inline XMFLOAT3 ScreenToViewportCoords(int x, int y)
{
	XMFLOAT3 ret;

	ret.x = (2.0f * x / g_RendererWidth) - 1.0f;
	ret.y = ((2.0f * y / g_RendererHeight) - 1.0f) * -1.0f;
	ret.z = 0.0f;

	return ret;
}

// Macros stolen from afxwin.h
#ifndef GET_X_LPARAM
#define GET_X_LPARAM(lp)                        ((int)(short)LOWORD(lp))
#endif
#ifndef GET_Y_LPARAM
#define GET_Y_LPARAM(lp)                        ((int)(short)HIWORD(lp))
#endif

// Helper macro to print streams to Visual Studio debug window 
// (only works if starting with the VS Debugger)
#define CONSOLEOUT( s )                     \
{                                           \
   std::wostringstream os_;			        \
   os_ << s << endl;                        \
   OutputDebugString( os_.str().c_str() );  \
}

#ifndef M_PI
#define M_PI 3.14159265358979323846f
#endif

#ifndef M_PI_2
#define M_PI_2     1.57079632679489661923f
#endif

#define RENDER_PANEL_ID 2001

//Constraints
#define SCALE_MAX_BOUND 5.0f

// Button IDs
enum class ButtonID
{
	BTN_ID_TETRAHEDRON = 1001,
	BTN_ID_CUBE,
	BTN_ID_CYLINDER,
	BTN_ID_SPHERE,
	BTN_ID_UNION,
	BTN_ID_INTERSECTION,
	BTN_ID_DIFFERENCE_AB,
	BTN_ID_DIFFERENCE_BA,
	BTN_ID_UNDO,
	BTN_ID_COPY,
	BTN_ID_DELETE,
	BTN_COUNT
};

inline int GetButtonCount() { return (int)ButtonID::BTN_COUNT - (int)ButtonID::BTN_ID_TETRAHEDRON; }

//Key codes...

#define VK_OEM3 0xC0 //Tilda ~
#define VK_0	0x30
#define VK_1	0x31
#define VK_2	0x32
#define VK_3	0x33
#define VK_4	0x34
#define VK_5	0x35
#define VK_6	0x36
#define VK_7	0x37
#define VK_8	0x38
#define VK_9	0x39
///////
#define VK_A	0x041
#define VK_B	0x042
#define VK_C	0x043
#define VK_D	0x044
#define VK_E	0x045
#define VK_F	0x046
#define VK_G	0x047
#define VK_H	0x048
#define VK_I	0x049
#define VK_J	0x04A
#define VK_K	0x04B
#define VK_L	0x04C
#define VK_M	0x04D
#define VK_N	0x04E
#define VK_O	0x04F
#define VK_P	0x050
#define VK_Q	0x051
#define VK_R    0x052
#define VK_S	0x053
#define VK_T	0x054
#define VK_U	0x055
#define VK_V	0x056
#define VK_W	0x057
#define VK_X	0x058
#define VK_Y	0x059
#define VK_Z	0x05A

#endif // !CSG_DIRECTX_COMMON_H
