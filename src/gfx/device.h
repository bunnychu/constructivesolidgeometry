#ifndef CSG_DIRECTX_DEVICE_H
#define CSG_DIRECTX_DEVICE_H

#include <d3d11.h>
#include <d3d11.h>
//#include <d3dx11.h>
//#include <d3dx10.h>

class Device 
{
public:
	bool isUsingDepthBuffer;

	IDXGISwapChain* swapChain;

	ID3D11Device* device;
	ID3D11DeviceContext* deviceContext;

	// Render target
	ID3D11RenderTargetView* renderTargetView;
	ID3D11Texture2D* backBuffer;

	// Depth stencil
	ID3D11DepthStencilView* depthStencilView;
	ID3D11Texture2D* depthStencilBuffer;

private:
	static Device* instance;
	Device () : isUsingDepthBuffer(true) {}

public:
	static Device& GetInstance()
	{
		if (instance == nullptr)
			instance = new Device();

		return *instance;
	}

	bool Init(HINSTANCE hInstance, HWND hwnd, int width, int height);
	void ReleaseObjects();

	void UseDepthBuffer(bool value);
};

#endif // !CSG_DIRECTX_DEVICE_H