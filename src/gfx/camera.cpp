#include "common.h"
#include "camera.h"
#include "input/inputhandler.h"

CSG_BEGIN_NAMESPACE

Camera::Camera()
{
	inputHandler = new CameraInputHandler(this);

	float fovY = 0.25f * 3.14f;
	float aspectRatio = static_cast<float>(g_RendererWidth) / g_RendererHeight;
	float nearZ = 0.1f;
	float farZ = 100.0f;

	SetProjection(fovY, aspectRatio, nearZ, farZ);

	// Creating the viewport
	ZeroMemory(&viewport, sizeof(viewport));

	viewport.TopLeftX = 0.f;
	viewport.TopLeftY = 0.f;
	viewport.Width = static_cast<float>(g_RendererWidth);
	viewport.Height = static_cast<float>(g_RendererHeight);
	viewport.MinDepth = 0.f;
	viewport.MaxDepth = 1.f;

	// Set the viewport to the RS
	Device::GetInstance().deviceContext->RSSetViewports(1, &viewport);

	ZeroMemory(&wireframeDesc, sizeof(wireframeDesc));
	wireframeDesc.CullMode = D3D11_CULL_BACK;
	wireframeDesc.FillMode = D3D11_FILL_WIREFRAME;

	ZeroMemory(&msaaDesc, sizeof(msaaDesc));
	msaaDesc.CullMode = D3D11_CULL_BACK;
	msaaDesc.FillMode = D3D11_FILL_SOLID;
	msaaDesc.MultisampleEnable = true;
	msaaDesc.AntialiasedLineEnable = true;

	Device::GetInstance().device->CreateRasterizerState(&wireframeDesc, &wireframe);
	Device::GetInstance().device->CreateRasterizerState(&msaaDesc, &msaa);

	Device::GetInstance().deviceContext->RSSetState(msaa);

	wireframeEnabled = false;
}

Camera::~Camera()
{
}

void Camera::SetProjection(float _fovY, float _aspectRatio, float _nearZ, float _farZ)
{
	fovY = _fovY;
	aspectRatio = _aspectRatio;
	nearZ = _nearZ;
	farZ = _farZ;

	projection = XMMatrixPerspectiveFovLH(fovY, aspectRatio, nearZ, farZ);
}

XMMATRIX Camera::GetView() const
{
	XMMATRIX inverseView = transform.GetRotationMat() * transform.GetTranslationMat();
	XMVECTOR det = XMMatrixDeterminant(inverseView);
	return XMMatrixInverse(&det, inverseView);
}

XMMATRIX Camera::GetInverseView() const
{
	return transform.GetRotationMat() * transform.GetTranslationMat();
}

XMMATRIX Camera::GetInverseViewProjection() const
{
	XMMATRIX view = GetView();
	XMMATRIX viewProj = view * projection;
	XMVECTOR det = XMMatrixDeterminant(viewProj);
		
	return XMMatrixInverse(&det, viewProj);
}

void Camera::SetWireframeEnabled(bool value)
{
	if (wireframeEnabled != value)
	{
		wireframeEnabled = value;
		Device::GetInstance().deviceContext->RSSetState(wireframeEnabled == true ? wireframe : msaa);
	}
}

void Camera::Release()
{
	wireframe->Release();
}

CSG_END_NAMESPACE