#ifndef CSG_CSG_PLANE_H
#define CSG_CSG_PLANE_H

#include "gfx/common.h"

CSG_BEGIN_NAMESPACE

class Polygon;

enum class Placement
{
	Coplanar = 0,
	Front = 1,
	Back = 2,
	Intersecting = 3
};

class Plane
{
	XMFLOAT3 normal; 
	float w;

public:
	Plane(XMFLOAT3 _normal, float _w);
	Plane(XMFLOAT3 x, XMFLOAT3 y, XMFLOAT3 z);
	Plane(Vertex x, Vertex y, Vertex z);

	void Flip();

	// Return a point's position relative to this plane
	Placement CheckPoint(const XMFLOAT3& pos);
	Placement CheckPoint(const Vertex& v);

	float GetPointToPlaneDistance(const XMFLOAT3& pos);

	void SplitPolygon(const Polygon& polygon,
					  vector<Polygon>& coplanarFront,
					  vector<Polygon>& coplanarBack,
					  vector<Polygon>& front,
					  vector<Polygon>& back);
	
	// Getters
	inline XMFLOAT3 GetNormal() const { return normal; }
	inline float GetW() const { return w; }
};

CSG_END_NAMESPACE
#endif //CSG_CSG_PLANE_H