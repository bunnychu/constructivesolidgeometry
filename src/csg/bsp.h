#ifndef CSG_CSG_BSP_H
#define CSG_CSG_BSP_H

#include "gfx/common.h"
#include "csg/polygon.h"
#include "primitives/drawable.h"

CSG_BEGIN_NAMESPACE

class Plane;
struct BSPNode;

class BSPTree
{
	BSPNode* root;
};

struct BSPNode
{
	BSPNode* back;
	BSPNode* front;

	Plane* plane;
    
	vector<Polygon> coplanarBack;
	vector<Polygon> coplanarFront;

	BSPNode();
	BSPNode(const vector<Polygon>& polygons);

	void Create(const vector<Polygon>& polygons);
	vector<Polygon> ReturnOutsidePolygonsOf(const vector<Polygon>& polygons) const;
	vector<Polygon> ReturnInsidePolygonsOf(const vector<Polygon>& polygons) const;

	vector<Polygon> GetAllPolygons() const;
};
 
BSPNode* UnionCSG(const BSPNode& a, const BSPNode& b);
BSPNode* IntersectionCSG(const BSPNode& a, const BSPNode& b);
BSPNode* SubstractionCSG(const BSPNode& a, const BSPNode& b);
BSPNode* InvSubstractionCSG(const BSPNode& a, const BSPNode& b);


CSG_END_NAMESPACE
#endif //CSG_CSG_BSP_H