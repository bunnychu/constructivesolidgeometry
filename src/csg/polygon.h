#ifndef CSG_CSG_POLYGON_H
#define CSG_CSG_POLYGON_H

#include "gfx/common.h"
#include "csg/plane.h"

CSG_BEGIN_NAMESPACE

class Polygon
{
public:
	vector<Vertex> vertices;
	Plane plane;

	Polygon(vector<Vertex> _vertices);
	~Polygon();
	void Flip();

	// Test the intersection between ray and polygon
	// and return the distance to the intersection
	bool TestRayIntersection(XMFLOAT3 position, XMFLOAT3 direction);
};

CSG_END_NAMESPACE
#endif CSG_CSG_POLYGON_H