#include "plane.h"
#include "csg/polygon.h"

CSG_BEGIN_NAMESPACE

Plane::Plane(XMFLOAT3 _normal, float _w)
	: w(_w)
{
	normal = Normalize(_normal);
}

Plane::Plane(XMFLOAT3 x, XMFLOAT3 y, XMFLOAT3 z)
{
	normal = Normalize(Cross(y - x, z - x));
	w = Dot(x, normal);
}

Plane::Plane(Vertex x, Vertex y, Vertex z) : Plane(x.pos, y.pos, z.pos)
{
}

void Plane::Flip()
{
	normal = -normal;
	w = -w;
}

Placement Plane::CheckPoint(const XMFLOAT3& pos)
{
	float pointProjection = Dot(normal, pos);
	
	// Compare point projection onto plane normal vs W
	// pointProj > w  => front
	// pointProj < w  => back

	if (pointProjection - w > EPSILON)
	{
		return Placement::Front;
	}
	
	if (pointProjection - w < -EPSILON)
	{
		return Placement::Back;
	}
	
	return Placement::Coplanar;
}

Placement Plane::CheckPoint(const Vertex& v)
{
	return CheckPoint(v.pos);
}

float Plane::GetPointToPlaneDistance(const XMFLOAT3& pos)
{
	float pointProjection = Dot(normal, pos);

	return abs(pointProjection - w) < EPSILON ? 0.0f : pointProjection - w;
}

void Plane::SplitPolygon(const Polygon& polygon, vector<Polygon>& coplanarFront, vector<Polygon>& coplanarBack, vector<Polygon>& front, vector<Polygon>& back)
{
	//The placement of the entire Polygon (the OR of all of its vertices)
	Placement placement = Placement::Coplanar;

	//The placement of each vertex
	vector<Placement> vertexPlacement;

	for (auto& v : polygon.vertices)
	{
		vertexPlacement.push_back(CheckPoint(v.pos));
		placement = static_cast<Placement>((int)placement | (int)vertexPlacement.back());
	}

	switch (placement)
	{
	case Placement::Coplanar:
		{
			//is it coplanar front or coplanar back
			float normalsDot = Dot(normal, polygon.plane.GetNormal());
			
			if (normalsDot + EPSILON > 1.0f)
				coplanarFront.push_back(polygon);
			else if (normalsDot - EPSILON < -1.0f)
			{
				coplanarBack.push_back(polygon);
			}
		}
		break;
	case Placement::Front:
		{
			front.push_back(polygon);
		}
		break;
	case Placement::Back:
		{
			back.push_back(polygon);
		}
		break;
	case Placement::Intersecting:
		{
			//Polygon x;
			//Polygon y; 
			vector<Vertex> polyFront;
			vector<Vertex> polyBack;

			for (int curIdx = 0; curIdx < polygon.vertices.size(); curIdx++)
			{
				int nextIdx = (curIdx + 1) % polygon.vertices.size();
				
				Vertex curVertex = polygon.vertices[curIdx];
				Vertex nextVertex = polygon.vertices[nextIdx];

				if (vertexPlacement[curIdx] == Placement::Front) polyFront.push_back(curVertex);
				if (vertexPlacement[curIdx] == Placement::Back) polyBack.push_back(curVertex);
				
				// A coplanar vertex goes in both polys (front & back)
				if (vertexPlacement[curIdx] == Placement::Coplanar)
				{
					polyFront.push_back(curVertex);
					polyBack.push_back(curVertex);
				}

				bool intersecting = static_cast<Placement>((int)vertexPlacement[curIdx] | (int)vertexPlacement[nextIdx]) == Placement::Intersecting;
				
				if (intersecting)
				{
					float distanceTotal = Dot(normal, (nextVertex.pos - curVertex.pos));
					float distanteCurrentToPlane = w - Dot(normal, curVertex.pos);
					float t = distanteCurrentToPlane / distanceTotal;

					Vertex v = Lerp(curVertex, nextVertex, t);
					
					//Intersecting point goes in both polys
					polyFront.push_back(v);
					polyBack.push_back(v);
				}
			}

			front.push_back(Polygon(polyFront));
			back.push_back(Polygon(polyBack));
		}
		break;
	default:
		break;
	}
}

CSG_END_NAMESPACE