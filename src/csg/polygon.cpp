#include "polygon.h"
#include <cstdlib> // for rand()
#include "..\gfx\scene.h"

CSG_BEGIN_NAMESPACE

Polygon::Polygon(vector<Vertex> _vertices) 
	: vertices(_vertices)
	, plane(_vertices[0], _vertices[1], _vertices[2])
{
	// Temp hack for random colors on polygons
	XMFLOAT4 randomColor;
 	randomColor.x = (rand() % 255) / 256.0f;
 	randomColor.y = (rand() % 255) / 256.0f;
 	randomColor.z = (rand() % 255) / 256.0f;
	
	randomColor.w = 1.0f;

	for (auto& v : vertices)
	{
		v.color = randomColor;
	}

	if (vertices.size() < 3)
	{
		MessageBoxA(NULL, "Polygon vertex count lower than 3", "WARNING", MB_OK);
	}
}

Polygon::~Polygon()
{
}

void Polygon::Flip()
{
	Vertex temp;
	size_t j = 0;

	for (int i = 0; i < vertices.size() / 2; i++)
	{
		j = vertices.size() - i - 1;
		temp = vertices[i];
		vertices[i] = vertices[j];
		vertices[j] = temp;
	}

	plane.Flip();
}

bool CSG::Polygon::TestRayIntersection(XMFLOAT3 position, XMFLOAT3 direction)
{
	//Discard tests in which the polygon is facing away from the ray
	if (Dot(direction, plane.GetNormal()) >= 0)
		return false;

	XMFLOAT3 A = vertices[0].pos;
	XMFLOAT3 B = vertices[1].pos;
	XMFLOAT3 C = vertices[2].pos;

	XMFLOAT3 AB = B - A;
	XMFLOAT3 AC = C - A;

	XMFLOAT3 N = Cross(AB, AC);

	XMFLOAT4X4 transformation;

	transformation.m[0][0] = AB.x;
	transformation.m[0][1] = AB.y;
	transformation.m[0][2] = AB.z;
	transformation.m[0][3] = 0.0f;

	transformation.m[1][0] = AC.x;
	transformation.m[1][1] = AC.y;
	transformation.m[1][2] = AC.z;
	transformation.m[1][3] = 0.0f;

	transformation.m[2][0] = N.x;
	transformation.m[2][1] = N.y;
	transformation.m[2][2] = N.z;
	transformation.m[2][3] = 0.0f;

	transformation.m[3][0] = A.x;
	transformation.m[3][1] = A.y;
	transformation.m[3][2] = A.z;
	transformation.m[3][3] = 1.0f;

	XMMATRIX matrix = XMLoadFloat4x4(&transformation);
	XMVECTOR det = XMMatrixDeterminant(matrix);

	XMMATRIX invMatrix = XMMatrixInverse(&det, matrix);

// 	XMFLOAT3 transformedA = invMatrix * A; //must be (0 0 0)
// 	XMFLOAT3 transformedB = invMatrix * B; //must be (1 0 0)
// 	XMFLOAT3 transformedC = invMatrix * C; //must be (0 1 0)

	XMFLOAT4 transformedPoint = invMatrix * XMFLOAT4(position.x, position.y, position.z, 1.0f);
	XMFLOAT4 transformedDirection = invMatrix * XMFLOAT4(direction.x, direction.y, direction.z, 0.0f);

	float t = -transformedPoint.z / transformedDirection.z;
	float u = transformedPoint.x  + t * transformedDirection.x;
	float v = transformedPoint.y + t * transformedDirection.y;


	return u >= 0 && v >= 0 && (u + v) <= 1 && t >= 0;
}

CSG_END_NAMESPACE


