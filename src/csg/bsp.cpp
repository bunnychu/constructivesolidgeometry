#include "bsp.h"
#include "csg/plane.h"

CSG_BEGIN_NAMESPACE

BSPNode::BSPNode() : 
	back(nullptr), 
	front(nullptr),
	plane(nullptr)
{
}

BSPNode::BSPNode(const vector<Polygon>& polygons) : BSPNode()
{
	Create(polygons);
}

void BSPNode::Create(const vector<Polygon>& polygons)
{
	if (polygons.size() == 0) return;
	if (plane == nullptr)	  plane = new Plane(polygons[0].plane);

	vector<Polygon> frontPolys;
	vector<Polygon> backPolys;

	for (const Polygon& p : polygons)
	{
		plane->SplitPolygon(p, coplanarFront, coplanarBack, frontPolys, backPolys);
	}

	if (frontPolys.size() != 0)
	{
		if (front == nullptr) front = new BSPNode();
		front->Create(frontPolys);
	}

	if (backPolys.size() != 0)
	{
		if (back == nullptr) back = new BSPNode();
		back->Create(backPolys);
	}
}

vector<Polygon> BSPNode::ReturnOutsidePolygonsOf(const vector<Polygon>& polygons) const
{
	vector<Polygon> returnPolys;
	vector<Polygon> frontPolys;
	vector<Polygon> backPolys;

	for (const Polygon& p : polygons)
	{
		plane->SplitPolygon(p, backPolys, backPolys, frontPolys, backPolys);
	}

	if (frontPolys.size() != 0)	returnPolys = frontPolys;

	if (backPolys.size() != 0 && back != nullptr)
	{
		vector<Polygon> otherOutsidePolys;
		otherOutsidePolys = back->ReturnOutsidePolygonsOf(backPolys);
		returnPolys.insert(returnPolys.end(), otherOutsidePolys.begin(), otherOutsidePolys.end());
	}

	return returnPolys;
}

vector<Polygon> BSPNode::ReturnInsidePolygonsOf(const vector<Polygon>& polygons) const
{
	vector<Polygon> frontPolys;
	vector<Polygon> backPolys;

	for (const Polygon& p : polygons)
	{
		plane->SplitPolygon(p, backPolys, backPolys, frontPolys, backPolys);
	}

	//if (frontPolys.size() != 0)	front->RemovePolysInsideOf(frontPolys);

	if (backPolys.size() != 0 && back != nullptr)
	{
		return back->ReturnInsidePolygonsOf(backPolys);
	}

	return backPolys;
}

vector<Polygon> BSPNode::GetAllPolygons() const
{
	vector<Polygon> result;
	result.insert(result.end(), coplanarBack.begin(), coplanarBack.end());
	result.insert(result.end(), coplanarFront.begin(), coplanarFront.end());

	if (front != nullptr)
	{
		vector<Polygon> frontChildPolys = front->GetAllPolygons();
		result.insert(result.end(), frontChildPolys.begin(), frontChildPolys.end());
	}

	if (back != nullptr)
	{
		vector<Polygon> backChildPolys = back->GetAllPolygons();
		result.insert(result.end(), backChildPolys.begin(), backChildPolys.end());
	}

	return result;
}

BSPNode* UnionCSG(const BSPNode& a, const BSPNode& b)
{
	BSPNode* result = nullptr;
	vector<Polygon> resultingPolygons;

 	vector<Polygon> x = a.ReturnOutsidePolygonsOf(b.GetAllPolygons());
 	vector<Polygon> y = b.ReturnOutsidePolygonsOf(a.GetAllPolygons());

	resultingPolygons.insert(resultingPolygons.end(), x.begin(), x.end());
	resultingPolygons.insert(resultingPolygons.end(), y.begin(), y.end());

	if (resultingPolygons.size() > 0)
	{
		result = new BSPNode(resultingPolygons);
	}

	return result;
}

BSPNode* IntersectionCSG(const BSPNode& a, const BSPNode& b)
{
	BSPNode* result = nullptr;
	vector<Polygon> resultingPolygons;

	vector<Polygon> x = a.ReturnInsidePolygonsOf(b.GetAllPolygons());
	vector<Polygon> y = b.ReturnInsidePolygonsOf(a.GetAllPolygons());
	
	resultingPolygons.insert(resultingPolygons.end(), x.begin(), x.end());
	resultingPolygons.insert(resultingPolygons.end(), y.begin(), y.end());

	if (resultingPolygons.size() > 0)
	{
		result = new BSPNode(resultingPolygons);
	}

	return result;
}

BSPNode* SubstractionCSG(const BSPNode& a, const BSPNode& b)
{
	BSPNode* result = nullptr;

	vector<Polygon> resultingPolygons;

	vector<Polygon> x = b.ReturnOutsidePolygonsOf(a.GetAllPolygons());
	vector<Polygon> y = a.ReturnInsidePolygonsOf(b.GetAllPolygons());

	for (Polygon& p : y)
	{
		p.Flip();
	}

	resultingPolygons.insert(resultingPolygons.end(), x.begin(), x.end());
	resultingPolygons.insert(resultingPolygons.end(), y.begin(), y.end());

	if (resultingPolygons.size() > 0)
	{
		result = new BSPNode(resultingPolygons);
	}

	return result;
}

BSPNode* InvSubstractionCSG(const BSPNode& a, const BSPNode& b)
{
	return SubstractionCSG(b, a);
}

CSG_END_NAMESPACE

