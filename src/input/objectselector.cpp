#include "objectselector.h"
#include "primitives/drawable.h"
#include "gfx/camera.h"
#include "primitives/cube.h"
#include "gfx/scene.h"
#include "csg/polygon.h"
#include "primitives/debugray.h"
#include "primitives/containers/controllerbuilder.h"

CSG_BEGIN_NAMESPACE

ObjectSelector ObjectSelector::instance;

ObjectSelector::ObjectSelector() : 
	camera(nullptr),
	controllers {0} // initialize the whole controller pointer array to zero
{
}

CSG::ObjectSelector::~ObjectSelector()
{
	for (int i = 0; i < ControllerType::CT_COUNT; i++)
	{
		if (controllers[i])
			delete controllers[i];
	}
}

ObjectSelector& ObjectSelector::GetInstance()
{
	return instance;
}

void ObjectSelector::Init()
{
	// Create the move rotate scale UI controllers
	controllers[ControllerType::CT_MOVE] = ControllerBuilder::BuildMoveController();
	controllers[ControllerType::CT_ROTATE] = ControllerBuilder::BuildRotateController();
	controllers[ControllerType::CT_SCALE] = ControllerBuilder::BuildScaleController();

	controllers[ControllerType::CT_MOVE]->SetIsVisible(false);
	controllers[ControllerType::CT_ROTATE]->SetIsVisible(false);
	controllers[ControllerType::CT_SCALE]->SetIsVisible(false);

	activeController = controllers[ControllerType::CT_MOVE];
}

void ObjectSelector::UseCamera(Camera* _camera)
{
	camera = _camera;
}

Drawable* ObjectSelector::SelectObjectAtPosition(int x, int y)
{
	XMFLOAT3 viewportCoords = ScreenToViewportCoords(x, y);
	XMFLOAT4 ndcCoords;

	ndcCoords.x = viewportCoords.x;
	ndcCoords.y = viewportCoords.y;
	ndcCoords.z = 0.0f;
	ndcCoords.w = 1.0f;

	// Transform from viewport to world coordinates
	ndcCoords = camera->GetInverseViewProjection() * ndcCoords;

	XMFLOAT3 worldPointNear;

	// Divide by w to get world space point
	worldPointNear.x = ndcCoords.x / ndcCoords.w;
	worldPointNear.y = ndcCoords.y / ndcCoords.w;
	worldPointNear.z = ndcCoords.z / ndcCoords.w;

	ndcCoords.x = viewportCoords.x;
	ndcCoords.y = viewportCoords.y;
	ndcCoords.z = 1.0f;
	ndcCoords.w = 1.0f;

	ndcCoords = camera->GetInverseViewProjection() * ndcCoords;

	XMFLOAT3 worldPointFar;

	worldPointFar.x = ndcCoords.x / ndcCoords.w;
	worldPointFar.y = ndcCoords.y / ndcCoords.w;
	worldPointFar.z = ndcCoords.z / ndcCoords.w;

	XMFLOAT3 direction = worldPointFar - worldPointNear;
	direction = Normalize(direction);

//TODO: FIX THIS CRASH
 /*	static DebugRay* debugRay = nullptr;
 	
 	if (debugRay) delete debugRay;
 
 	debugRay = new DebugRay(worldPointNear, direction);
*/
	Drawable* selectedTarget = nullptr;
	float distanceToPlane = 10000.0f;

	for (auto& drawable : Scene::GetInstance().GetAllDrawables())
	{
		// Skip checking this drawable if it is invisible
		if (drawable->GetIsVisible() == false)
			continue;

		for (auto& polygon : drawable->ToPolygons())
		{
			if (polygon.TestRayIntersection(worldPointNear, direction))
			{
				if (polygon.plane.GetPointToPlaneDistance(worldPointNear) < distanceToPlane)
				{
					distanceToPlane = polygon.plane.GetPointToPlaneDistance(worldPointNear);
					selectedTarget = drawable;
				}
			}
		}
	}

	return selectedTarget;
}

void ObjectSelector::AddSelectedObject(Drawable* drawable, bool withClear/* = true*/)
{
	if (withClear || drawable == nullptr)
		selectedObjects.clear();

	if (drawable && find(selectedObjects.begin(), selectedObjects.end(), drawable) == selectedObjects.end())
	{
		selectedObjects.push_back(drawable);
	}

	UpdateActiveControllerTransform();
}

void ObjectSelector::SetActiveController(ControllerType controllerType)
{
	// Early out if this controller is selected
	if (activeController == controllers[controllerType])
		return;

	controllers[controllerType]->SetIsVisible(activeController->GetIsVisible());
	controllers[controllerType]->GetTransform().SetPosition(activeController->GetTransform().GetPosition());
	activeController->SetIsVisible(false);

	activeController = controllers[controllerType];
}

void ObjectSelector::UpdateActiveControllerTransform()
{
	if (selectedObjects.size() == 0)
	{
		//moveController->GetTransform().SetPosition(objPosition);
		activeController->SetIsVisible(false);
		return;
	}

	// Calculate bounding volume for selected objects position
	// and set the controller to its center
	BoundingVolume selectionBV;

	selectionBV.minBound = selectedObjects[0]->GetTransform().GetPositionFloat3();
	selectionBV.maxBound = selectedObjects[0]->GetTransform().GetPositionFloat3();

	for (int i = 1; i < selectedObjects.size(); i++)
	{
		XMFLOAT3 objPosition = selectedObjects[i]->GetTransform().GetPositionFloat3();
		
		if (selectionBV.minBound.x > objPosition.x) selectionBV.minBound.x = objPosition.x;
		if (selectionBV.minBound.y > objPosition.y) selectionBV.minBound.y = objPosition.y;
		if (selectionBV.minBound.z > objPosition.z) selectionBV.minBound.z = objPosition.z;

		if (selectionBV.maxBound.x < objPosition.x) selectionBV.maxBound.x = objPosition.x;
		if (selectionBV.maxBound.y < objPosition.y) selectionBV.maxBound.y = objPosition.y;
		if (selectionBV.maxBound.z < objPosition.z) selectionBV.maxBound.z = objPosition.z;
	}

 	activeController->GetTransform().SetPosition(selectionBV.GetCenter());
 	activeController->SetIsVisible(true);
}

void ObjectSelector::UpdateSelectedObjects(XMFLOAT3 offset)
{
	for (auto& selectedObject : selectedObjects)
	{
		if (activeController == controllers[ControllerType::CT_MOVE])
		{
			XMFLOAT3 objPosition = selectedObject->GetTransform().GetPositionFloat3();
			selectedObject->GetTransform().SetPosition(objPosition + offset);
		}

		if (activeController == controllers[ControllerType::CT_ROTATE])
		{
			XMFLOAT3 objRotation = selectedObject->GetTransform().GetRotationFloat3();
			selectedObject->GetTransform().SetRotation(objRotation + offset);
		}

		if (activeController == controllers[ControllerType::CT_SCALE])
		{
			XMFLOAT3 objScale = selectedObject->GetTransform().GetScaleFloat3();
			selectedObject->GetTransform().SetScale(objScale + offset);
		}
	}
}

void ObjectSelector::CloneSelectedObjects()
{
	// Clone selected objects
	for (auto& d : selectedObjects)
	{
		Drawable* newDrawable = new Drawable(d->ToPolygons());
		newDrawable->SetColor(d->GetColor());
		BoundingVolume bv = d->GetBoundingVolume();
		XMFLOAT3 targetPosition = d->GetTransform().GetPositionFloat3() + XMFLOAT3(bv.maxBound.x - bv.minBound.x + 0.2f, 0.0f, 0.0f);
		newDrawable->GetTransform().SetPosition(targetPosition);
	}
}

void ObjectSelector::DeleteSelectedObjects()
{
	// Delete selected objects
	for (auto* d : selectedObjects)
	{
		Scene::GetInstance().DeleteObject(d);
	}

	//Clear object selection
	AddSelectedObject(nullptr);
}

bool ObjectSelector::IsSelectionActive()
{
	return selectedObjects.size() > 0 && activeController != nullptr && activeController->GetIsVisible() == true;
}

CSG_END_NAMESPACE


