#ifndef CSG_INPUT_OBJECTSELECTOR_H
#define CSG_INPUT_OBJECTSELECTOR_H

#include "gfx/common.h"
#include "primitives/containers/drawablecontainer.h"

CSG_BEGIN_NAMESPACE

class Camera;
class Drawable;

enum ControllerType
{
	CT_MOVE = 0,
	CT_ROTATE = 1,
	CT_SCALE = 2,
	CT_COUNT = 3,
};

class ObjectSelector
{
private:
	ObjectSelector();
	~ObjectSelector();
	static ObjectSelector instance;

	// A list of the selected objects
	vector<Drawable*> selectedObjects;

public:
	DrawableContainer* controllers[ControllerType::CT_COUNT];
	
	DrawableContainer* activeController;

	static ObjectSelector& GetInstance();

	void Init();

	void UseCamera(Camera* _camera);

	// Pick an object from the scene based on screen coordinates
	Drawable* SelectObjectAtPosition(int x, int y);

	void AddSelectedObject(Drawable* drawable, bool withClear = true);
	inline vector<Drawable*> GetSelectedObjects() { return selectedObjects; }

	void SetActiveController(ControllerType controllerType);

	void UpdateActiveControllerTransform();
	void UpdateSelectedObjects(XMFLOAT3 offset);

	void CloneSelectedObjects();
	void DeleteSelectedObjects();

	bool IsSelectionActive();

protected:
	Camera* camera;
};

CSG_END_NAMESPACE

#endif//CSG_INPUT_OBJECTSELECTOR_H