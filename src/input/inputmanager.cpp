#include "inputmanager.h"
#include "primitives/drawable.h"

CSG_BEGIN_NAMESPACE

InputManager InputManager::instance;

InputManager::InputManager()
{
}

InputManager& InputManager::GetInstance()
{
	return instance;
}

void InputManager::ProcessKeyMessage(WPARAM key, UINT keyState)
{
	//TODO: investigate this bug, too many objects registered
	for (auto& client : clients)
	{
		client->OnKeyPress(key, keyState);
	}
}

void InputManager::ProcessMouseKeyMessage(WPARAM key, UINT keyState)
{
	for (auto& client : clients)
	{
		client->OnMousePress(key, keyState);
	}
}

void InputManager::ProcessMouseMoveMessage(WPARAM key, UINT keyState)
{
	for (auto& client : clients)
	{
		client->OnMouseMove(key, keyState);
	}
}

void InputManager::Register(Drawable* drawable)
{
	clientsToAdd.push_back(drawable);
}

void InputManager::Unregister(Drawable* drawable)
{
	clientsToRemove.push_back(drawable);
}

void CSG::InputManager::OnFrameStart()
{
	if (clientsToAdd.size())
	{
		clients.insert(clients.end(), clientsToAdd.begin(), clientsToAdd.end());
	}

	if (clientsToRemove.size())
	{
		for (Drawable* drawable : clientsToRemove)
		{
			clients.erase(remove(clients.begin(), clients.end(), drawable), clients.end());
		}
	}

	clientsToAdd.clear();
	clientsToRemove.clear();
}
CSG_END_NAMESPACE


