#include "input/inputhandler.h"
#include "input/objectselector.h"
#include "gfx/camera.h"

CSG_BEGIN_NAMESPACE

const float KEY_MOVE_SPEED = 0.2f;
const float KEY_SCALE_SPEED = 0.1f;
const float MOUSE_MOVE_SPEED = 0.005f;
const float OBJECT_ROTATE_SPEED = float(M_PI) / 12.0f;

// Set the cursor position at the (x,y) position in the client (window) area
void SetCursorPosInClientXY(int x, int y)
{
	POINT p;
	p.x = x;
	p.y = y;
	ClientToScreen(g_Hwnd, &p);
	SetCursorPos(p.x, p.y);
}

POINT GetCursorPosInClientXY()
{
	POINT p;
	GetCursorPos(&p);
	ScreenToClient(g_Hwnd, &p);

	return p;
}

CameraInputHandler::CameraInputHandler(Camera* _camera) 
	: camera(_camera)
	, isRightMBDown(false)
	, mouseMoveX(0)
	, mouseMoveY(0)
{
}

void CameraInputHandler::OnKeyPress(WPARAM key, UINT keyState)
{
	//Parse global command keys (keys that perform an action regardless of selection state)
	switch (key)
	{
		// TOGGLE WIREFRAME
		case VK_OEM3:
		{
			camera->SetWireframeEnabled(!camera->GetWireframeEnabled());
		}
		break;

		case VK_C:
		{
			ObjectSelector::GetInstance().CloneSelectedObjects();
		}
		break;

		case VK_BACK:
		{
			ObjectSelector::GetInstance().DeleteSelectedObjects();
		}
		break;
		case VK_1:
		{
			ObjectSelector::GetInstance().SetActiveController(ControllerType::CT_MOVE);
		}
		break;

		case VK_2:
		{
			ObjectSelector::GetInstance().SetActiveController(ControllerType::CT_ROTATE);
		}
		break;

		case VK_3:
		{
			ObjectSelector::GetInstance().SetActiveController(ControllerType::CT_SCALE);
		}
		break;
	}

	//Early out this behavior if a controller is active
	if (ObjectSelector::GetInstance().IsSelectionActive())
		return;

	XMVECTOR position = camera->GetTransform().GetPosition();

	switch (key)
	{
		// MOVEMENT
		case VK_W:
		{
			position += XMVector3Transform(XMVectorSet(0.0f, 0.0f, KEY_MOVE_SPEED, 1.0f), camera->GetTransform().GetRotationMat());
		}
		break;

		case VK_S:
		{
			position += XMVector3Transform(XMVectorSet(0.0f, 0.0f, -KEY_MOVE_SPEED, 1.0f), camera->GetTransform().GetRotationMat());
		}
		break;

		case VK_A:
		{
			position += XMVector3Transform(XMVectorSet(-KEY_MOVE_SPEED, 0.0f, 0.0f, 1.0f), camera->GetTransform().GetRotationMat());
		}
		break;

		case VK_D:
		{
			position += XMVector3Transform(XMVectorSet(KEY_MOVE_SPEED, 0.0f, 0.0f, 1.0f), camera->GetTransform().GetRotationMat());
		}
		break;

		case VK_Q:
		{
			position += XMVector3Transform(XMVectorSet(0.0f, -KEY_MOVE_SPEED, 0.0f, 1.0f), camera->GetTransform().GetRotationMat());
		}
		break;

		case VK_E:
		{
			position += XMVector3Transform(XMVectorSet(0.0f, KEY_MOVE_SPEED, 0.0f, 1.0f), camera->GetTransform().GetRotationMat());
		}
		break;
	}

	camera->GetTransform().SetPosition(position);
}

void CameraInputHandler::OnMousePress(WPARAM key, UINT keyState)
{
	static POINT cursorPositionBeforePress;

	if (keyState == WM_RBUTTONDOWN)
	{
		ShowCursor(false);
		isRightMBDown = true;

		mouseMoveX = 0;
		mouseMoveY = 0;

		GetCursorPos(&cursorPositionBeforePress);
	}

	if (keyState == WM_RBUTTONUP)
	{
		ShowCursor(true);
		isRightMBDown = false;

		SetCursorPos(cursorPositionBeforePress.x, cursorPositionBeforePress.y);
	}

	if (keyState == WM_LBUTTONDOWN)
	{
		ObjectSelector::GetInstance().UseCamera(camera);
		
		POINT screenSpaceCoords = GetCursorPosInClientXY();
		Drawable* selectedObject = ObjectSelector::GetInstance().SelectObjectAtPosition(screenSpaceCoords.x, screenSpaceCoords.y);

		// ctrl key is down
		ObjectSelector::GetInstance().AddSelectedObject(selectedObject, GetKeyState(VK_CONTROL) < 0 ? false : true);
	}
}

void CameraInputHandler::OnMouseMove(WPARAM key, UINT keyState)
{
	if (!isRightMBDown)
	{
		return;
	}

	int windowCenterX = g_RendererWidth / 2;
	int windowCenterY = g_RendererHeight / 2;

	if (mouseMoveX == 0 && mouseMoveY == 0)
	{
		SetCursorPosInClientXY(windowCenterX, windowCenterY);
		mouseMoveX = windowCenterX;
		mouseMoveY = windowCenterY;
		return;
	}

	mouseMoveX = GET_X_LPARAM(key);
	mouseMoveY = GET_Y_LPARAM(key);

	static float offsetX = 0.0f;
	static float offsetY = 0.0f;

	if (mouseMoveX != windowCenterX || mouseMoveY != windowCenterY)
	{
		int deltaMouseMoveX = mouseMoveX - windowCenterX;
		int deltaMouseMoveY = mouseMoveY - windowCenterY;

		offsetX += deltaMouseMoveX * MOUSE_MOVE_SPEED;
		offsetY += deltaMouseMoveY * MOUSE_MOVE_SPEED;

		camera->GetTransform().SetRotation(offsetY, offsetX, 0.0f);
	}

	SetCursorPosInClientXY(windowCenterX, windowCenterY);
	return;
}

//----------------------------------------------------------------------------
MoveControllerInputHandler::MoveControllerInputHandler(DrawableContainer* _owner)
	: owner(_owner)
{
}

void MoveControllerInputHandler::OnKeyPress(WPARAM key, UINT keyState)
{
	if (!ObjectSelector::GetInstance().IsSelectionActive() || owner->GetIsVisible() == false)
		return;

	XMFLOAT3 position = owner->GetTransform().GetPositionFloat3();
	XMFLOAT3 offset(0.0f, 0.0f, 0.0f);

	switch (key)
	{
		// MOVEMENT
		case VK_W:
		{
			offset = offset + XMFLOAT3(0.0f, KEY_MOVE_SPEED, 0.0f);
		}
		break;

		case VK_S:
		{
			offset = offset + XMFLOAT3(0.0f, -KEY_MOVE_SPEED, 0.0f);
		}
		break;

		case VK_A:
		{
			offset = offset + XMFLOAT3(-KEY_MOVE_SPEED, 0.0f, 0.0f);
		}
		break;

		case VK_D:
		{
			offset = offset + XMFLOAT3(KEY_MOVE_SPEED, 0.0f, 0.0f);
		}
		break;

		case VK_Q:
		{
			offset = offset + XMFLOAT3(0.0f, 0.0f, -KEY_MOVE_SPEED);
		}
		break;

		case VK_E:
		{
			offset = offset + XMFLOAT3(0.0f, 0.0f, KEY_MOVE_SPEED);
		}
		break;

		default:
			return;
	}

	owner->GetTransform().SetPosition(position + offset);
	ObjectSelector::GetInstance().UpdateSelectedObjects(offset);
}

//----------------------------------------------------------------------------
RotateControllerInputHandler::RotateControllerInputHandler(DrawableContainer* _owner)
	: owner(_owner)
{
}

void RotateControllerInputHandler::OnKeyPress(WPARAM key, UINT keyState)
{
	if (!ObjectSelector::GetInstance().IsSelectionActive() || owner->GetIsVisible() == false)
		return;

	XMFLOAT3 rotation = owner->GetTransform().GetRotationFloat3();
	XMFLOAT3 offset(0.0f, 0.0f, 0.0f);

	switch (key)
	{
		case VK_W:
		{
			offset = offset + XMFLOAT3(0.0f, OBJECT_ROTATE_SPEED, 0.0f);
		}
		break;

		case VK_S:
		{
			offset = offset + XMFLOAT3(0.0f, -OBJECT_ROTATE_SPEED, 0.0f);
		}
		break;

		case VK_A:
		{
			offset = offset + XMFLOAT3(-OBJECT_ROTATE_SPEED, 0.0f, 0.0f);
		}
		break;

		case VK_D:
		{
			offset = offset + XMFLOAT3(OBJECT_ROTATE_SPEED, 0.0f, 0.0f);
		}
		break;

		case VK_Q:
		{
			offset = offset + XMFLOAT3(0.0f, 0.0f, -OBJECT_ROTATE_SPEED);
		}
		break;

		case VK_E:
		{
			offset = offset + XMFLOAT3(0.0f, 0.0f, OBJECT_ROTATE_SPEED);
		}
		break;

		default:
			return;
	}

	//owner->GetTransform().SetRotation(rotation + offset);
	ObjectSelector::GetInstance().UpdateSelectedObjects(offset);
}

//----------------------------------------------------------------------------
ScaleControllerInputHandler::ScaleControllerInputHandler(DrawableContainer* _owner)
	: owner(_owner)
{
}

void ScaleControllerInputHandler::OnKeyPress(WPARAM key, UINT keyState)
{
	if (!ObjectSelector::GetInstance().IsSelectionActive() || owner->GetIsVisible() == false)
		return;

	XMFLOAT3 scale = owner->GetTransform().GetScaleFloat3();
	XMFLOAT3 offset(0.0f, 0.0f, 0.0f);

	switch (key)
	{
		// MOVEMENT
		case VK_W:
		{
			offset = offset + XMFLOAT3(0.0f, KEY_SCALE_SPEED, 0.0f);
		}
		break;

		case VK_S:
		{
			offset = offset + XMFLOAT3(0.0f, -KEY_SCALE_SPEED, 0.0f);
		}
		break;

		case VK_A:
		{
			offset = offset + XMFLOAT3(-KEY_SCALE_SPEED, 0.0f, 0.0f);
		}
		break;

		case VK_D:
		{
			offset = offset + XMFLOAT3(KEY_SCALE_SPEED, 0.0f, 0.0f);
		}
		break;

		case VK_Q:
		{
			offset = offset + XMFLOAT3(0.0f, 0.0f, -KEY_SCALE_SPEED);
		}
		break;

		case VK_E:
		{
			offset = offset + XMFLOAT3(0.0f, 0.0f, KEY_SCALE_SPEED);
		}
		break;

		case VK_R:
		{
			offset = offset + XMFLOAT3(KEY_SCALE_SPEED, KEY_SCALE_SPEED, KEY_SCALE_SPEED);
		}
		break;

		case VK_F:
		{
			offset = offset + XMFLOAT3(-KEY_SCALE_SPEED, -KEY_SCALE_SPEED, -KEY_SCALE_SPEED);
		}
		break;

		default:
			return;
	}

	//owner->GetTransform().SetScale(rotation + offset);
	ObjectSelector::GetInstance().UpdateSelectedObjects(offset);
}

CSG_END_NAMESPACE
