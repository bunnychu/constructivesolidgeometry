#ifndef CSG_INPUT_INPUTMANAGER_H
#define CSG_INPUT_INPUTMANAGER_H

#include "gfx/common.h"

CSG_BEGIN_NAMESPACE

class Drawable;

class InputManager
{
private:
	InputManager();
	static InputManager instance;
	
	//TODO: this list could be broken down into an additional list of 'pending clients'
	//(clients that are queued to register), and register them at the begining of the frame

	vector<Drawable*> clients;
	vector<Drawable*> clientsToAdd;
	vector<Drawable*> clientsToRemove;

public:
	static InputManager& GetInstance();

	void ProcessKeyMessage(WPARAM key, UINT keyState);
	void ProcessMouseKeyMessage(WPARAM key, UINT keyState);
	void ProcessMouseMoveMessage(WPARAM key, UINT keyState);

	//Used by drawables to register / unregister
	void Register(Drawable* drawable);
	void Unregister(Drawable* drawable);

	//Commit register unregister at the end of the frame
	void OnFrameStart();
};

CSG_END_NAMESPACE
#endif //!CSG_INPUT_INPUTMANAGER_H
