#ifndef CSG_INPUT_INPUTHANDLER_H
#define CSG_INPUT_INPUTHANDLER_H

#include "gfx/common.h"

CSG_BEGIN_NAMESPACE

class Camera;
class DrawableContainer;

//--------------------------------------------------------
class InputHandler
{
public:
	InputHandler() {};
	virtual ~InputHandler() {};

	virtual void OnKeyPress(WPARAM key, UINT keyState) {}
	virtual void OnMousePress(WPARAM key, UINT keystate) {}
	virtual void OnMouseMove(WPARAM key, UINT keystate) {}
};

//--------------------------------------------------------
class CameraInputHandler : public InputHandler
{
private:
	Camera* camera;

	int mouseMoveX;
	int mouseMoveY;

	bool isRightMBDown;

public:
	CameraInputHandler(Camera* _camera);
	virtual void OnKeyPress (WPARAM key, UINT keyState) override;
	virtual void OnMousePress(WPARAM key, UINT keystate) override;
	virtual void OnMouseMove(WPARAM key, UINT keystate) override;
};

//--------------------------------------------------------
class MoveControllerInputHandler : public InputHandler
{
private:
	DrawableContainer* owner;

public:
	MoveControllerInputHandler(DrawableContainer* _owner);
	virtual void OnKeyPress(WPARAM key, UINT keyState) override;
	//virtual void OnMousePress(WPARAM key, UINT keystate) override;
	//virtual void OnMouseMove(WPARAM key, UINT keystate) override;
};

//--------------------------------------------------------
class RotateControllerInputHandler : public InputHandler
{
private:
	DrawableContainer* owner;

public:
	RotateControllerInputHandler(DrawableContainer* _owner);
	virtual void OnKeyPress(WPARAM key, UINT keyState) override;
	//virtual void OnMousePress(WPARAM key, UINT keystate) override;
	//virtual void OnMouseMove(WPARAM key, UINT keystate) override;
};

//--------------------------------------------------------
class ScaleControllerInputHandler : public InputHandler
{
private:
	DrawableContainer* owner;

public:
	ScaleControllerInputHandler(DrawableContainer* _owner);
	virtual void OnKeyPress(WPARAM key, UINT keyState) override;
	//virtual void OnMousePress(WPARAM key, UINT keystate) override;
	//virtual void OnMouseMove(WPARAM key, UINT keystate) override;
};

CSG_END_NAMESPACE
#endif //!CSG_INPUT_INPUTHANDLER_H
