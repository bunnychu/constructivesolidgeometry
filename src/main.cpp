#include "gfx/common.h"
#include "gfx/scene.h"
#include "input/inputmanager.h"
#include "ui/uimanager.h"

HWND g_Hwnd = nullptr;
HWND g_RendererHwnd = nullptr;

using namespace CSG;

void DrawScene();

bool InitializeWindow(HINSTANCE hInstance, int nShowCmd, int width, int height, bool windowed);
void MessageLoop();

LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);



int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd)
{
 	XMFLOAT3 v1 = { 0.0f, 3.0f, 4.0f };
 	XMFLOAT3 v2 = { 1.0f, 2.0f, 3.0f };
 	XMFLOAT3 add = XMFLOAT3(1.0f, 2.0f, 3.0f) + XMFLOAT3(1.0f, 2.0f, 3.0f);
 	XMFLOAT3 sub = v1-v2;
 	XMFLOAT3 inv = -v1;

	float test = Dot({ -1.0f, 0.0f, 0.0f }, { 0.0f, 0.0f, 1.0f });
 	float test1 = Length({ 0.0f, 3.0f, 4.0f });
 	XMFLOAT3 test2 = Normalize({ 0.0f, 3.0f, 4.0f });
	bool initResultWindow = InitializeWindow(hInstance, nShowCmd, g_WindowWidth, g_WindowHeight, true);
	bool initResultDevice = Device::GetInstance().Init(hInstance, g_RendererHwnd, g_RendererWidth, g_RendererHeight);

	if (!initResultWindow || !initResultDevice)
	{
		MessageBoxA(0, "Failed to initialize window or device", "Failed", MB_OK);
		return 0;
	}

	if (!Scene::GetInstance().Init())
	{
		MessageBoxA(NULL, "Failed to initialize scene", "Failed", MB_OK);
		return 0;
	}

	MessageLoop();
	return 0;
}

bool InitializeWindow(HINSTANCE hInstance, int nShowCmd, int width, int height, bool windowed)
{
	WNDCLASSEX wc;
	//ZeroMemory(&wc, sizeof(WNDCLASSEX));

	wc.cbSize = sizeof(WNDCLASSEX);
	wc.style = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc = WndProc;
	wc.cbClsExtra = NULL;
	wc.cbWndExtra = NULL;
	wc.hInstance = hInstance;
	wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)COLOR_WINDOW;
	wc.lpszMenuName = NULL;
	wc.lpszClassName = g_WndClassName;
	wc.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	if (!RegisterClassEx(&wc))
	{
		MessageBoxA(0, "Failed to register window class", "Failed", MB_OK);
		return false;
	}

	g_Hwnd = CreateWindowEx(NULL, g_WndClassName, g_WndClassName, WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN, CW_USEDEFAULT, CW_USEDEFAULT, width, height, NULL, NULL, hInstance, NULL);
	g_RendererHwnd = CreateWindowEx(NULL, g_WndClassName, g_WndClassName, WS_CHILD | WS_VISIBLE, 0, 0, g_RendererWidth, g_RendererHeight, g_Hwnd, (HMENU)RENDER_PANEL_ID, hInstance, NULL);

	if (!g_Hwnd)
	{
		MessageBoxA(0, "Failed to create window", "Failed", MB_OK);
		return false;
	}

	ShowWindow(g_Hwnd, nShowCmd);
	UpdateWindow(g_Hwnd);

	return true;
}

void MessageLoop()
{
	MSG msg;
	ZeroMemory(&msg, sizeof(MSG));

	while (true)
	{
		InputManager::GetInstance().OnFrameStart();
		Scene::GetInstance().OnFrameStart();

		if (PeekMessage(&msg, NULL, NULL, NULL, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				Scene::GetInstance().Release();
				return;
			}

			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		Scene::GetInstance().Update();
		DrawScene();
	}
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch (msg)
	{
		case WM_CREATE:
		{
			UIManager::GetInstance().CreateUIElements();
			break;
		}

		case WM_COMMAND:
		{
			
			if (HIWORD(wParam) == BN_CLICKED)
			{
				UIManager::GetInstance().HandleUIElementsInput(static_cast<ButtonID>(LOWORD(wParam)));
				SetFocus(g_RendererHwnd);
			}
			//return 0;
		}

		case WM_KEYDOWN:
		{
			if (wParam == VK_ESCAPE)
			{
// 				if (MessageBoxA(0, "Are you sure you want to exit?", "Really?", MB_YESNO | MB_ICONQUESTION) == IDYES)
// 				{
					DestroyWindow(hWnd);
//				}
			}

			InputManager::GetInstance().ProcessKeyMessage(wParam, msg);
			break;
		}

		case WM_LBUTTONDOWN:
		case WM_LBUTTONUP:
		case WM_RBUTTONDOWN:
		case WM_RBUTTONUP:
		{
			InputManager::GetInstance().ProcessMouseKeyMessage(lParam, msg);
			break;
		}

		case WM_MOUSEMOVE:
		{
			InputManager::GetInstance().ProcessMouseMoveMessage(lParam, msg);
			break;
		}

		case WM_DESTROY:
		{
			PostQuitMessage(0);
			return 0;
		}
	}

	return DefWindowProc(hWnd, msg, wParam, lParam);
}

void DrawScene()
{
	const XMFLOAT4 bgColor(0.0f, 0.3f, 0.8f, 1.0f);
	Device::GetInstance().deviceContext->ClearRenderTargetView(Device::GetInstance().renderTargetView, (const float*)(&bgColor));
	Device::GetInstance().deviceContext->ClearDepthStencilView(Device::GetInstance().depthStencilView, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
	
	Scene::GetInstance().Draw();

	Device::GetInstance().swapChain->Present(0, 0);
}
