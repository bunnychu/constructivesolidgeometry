TODO:
- Color Picker in UI for each drawable TBD

//List of topics covered

* Drawing with DirectX 
	* Initialization (swapchain, rendertarget, depthbuffer)
		- Directx functions start with acronims of the pipeline stage they are affecting
		
	RSSetViewport()
	IASetPrimitiveTopology()
	
	* Vertex Structure and Layout (Position, Normal, Color)
	* Constant buffers (perObjectCB)
	* Vertex Topologies Used
	* Programable shader pipeline
	* Transformation matrices
			- Object Transform
			- Camera View
			- Camera Projection
			- Vertex Transformation trough M * V * P
	* Vertex normals + Lambert Lighting
			
* Application architecture 
	- Main Application Loop: WinApiMsgLoop, Scene, InputManager, ObjectSelector
		* Explain each manager's role, structure, and how they all interact together in a class diagram
	
	- Drawable Primitives
		* Drawable Base Type
		* All derived base types 
		* Bounding volume calculation
		* Drawable to Polygon[]
		* Polygon[] to Drawable
			- Triangle fanning
	
* CSG Algorithm 
	- Polygon
	    * Present the contents of the struct & explained
		* Clockwise vertex ordering to determing polygon face
	- Plane
		* Plane equation 
		* Calculating plane equation from 3 points
		* Small utility methods like IsPointInPlane(..) and Flip().
		
	- Splitting a polygon by a plane
		*Algorithm 
		
	- BSP tree
		*BSP structure
		*Drawable to BSPNode
	
	- CSG Boolean operations.
		* Union
		* Intersection
		* Difference